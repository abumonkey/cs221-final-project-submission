import random
from game import Agent
from game import GameRules

class BaselineAgent( Agent ):

  def __init__(self, index):
    self.index = index # agent/player index {0,1,2,3}
    self.isKeyboardAgent = False
    self.simKeyStrokes = []
    self.lastSimKeyStrokes = []

  def getAgentName(self):
    return "BaselineAgent"

  def getAction(self, gameState):
    """
    To generate a actions, we need to define:
      + agentActions = gameState.getLegalActions(agentIndex)
      + succGameState = gameState.generateSuccessor(agentIndex, action)
      + gameState.isWin() # respect to agent 0
      + gameState.isLose() # respect to agent 0
    """
    def compareCosts(cost1, cost2):
      # return True if cost1 < cost2 else False
      # Comparisson is done by number of total gems.
      numGems1 = sum([gemCost[0] for gemCost in cost1.gemCostTuplesList])
      numGems2 = sum([gemCost[0] for gemCost in cost2.gemCostTuplesList])
      return numGems1 < numGems2

    def getCostInNumberOfGems(cost):
      return sum([gemCost[0] for gemCost in cost.gemCostTuplesList])

    def getActionToBuy(devCardToBuy, legalMoves):
      tier = -1
      column = -1
      zeroOrd = ord('0')
      for devCardTier in gameState.tableState.devCards:
        for i,devCard in enumerate(devCardTier):
          if devCard == devCardToBuy:
            tier = devCard.tier
            column = i+1
            return [zeroOrd+1, zeroOrd+tier, zeroOrd+column]

    def getActionToTake3DiffGems(legalMoves):
      possibleActions = []
      for legalMove in legalMoves:
        if legalMove[0] == ord('3'):
          possibleActions.append(legalMove)
      if len(possibleActions) == 0:
        return None
      return random.sample(possibleActions, 1)[0]

    def getActionToBuyAnyAffordableCard(legalMoves):
      possibleActions = []
      for legalMove in legalMoves:
        if legalMove[0] == ord('1'):
          possibleActions.append(legalMove)
      if len(possibleActions) == 0:
        return None
      return random.sample(possibleActions, 1)[0]

    def getActionToReserveAnyCardIfPossible(legalMoves):
      possibleActions = []
      for legalMove in legalMoves:
        if legalMove[0] == ord('4'):
          possibleActions.append(legalMove)
      if len(possibleActions) == 0:
        return None
      return random.sample(possibleActions, 1)[0]

    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions(self.index)
    # print "len(legalMoves)=", len(legalMoves)
    if len(legalMoves) == 0:
      self.simKeyStrokes = None
      self.lastSimKeyStrokes = None
      return None

    # Get cheapest with prestige
    # Init with tier 3, column 0 devCard. All tier 3 devCards have prestige > 0.
    cheaspestDevCard = gameState.tableState.devCards[2][0]
    for devCardTier in gameState.tableState.devCards:
      for devCard in devCardTier:
        if devCard is None or devCard.prestige == 0: continue
        if compareCosts(devCard.cost, cheaspestDevCard.cost):
          cheaspestDevCard = devCard
    # print "cheaspestDevCard(prestige>0)=", cheaspestDevCard

    # Get the devCard with the most prestige points.
    # can buy? buy
    # else next devCard with most prestige points.
    devCardList = []
    devCardCostList = []
    gameRules = GameRules()
    devCardToBuy = None
    action = None
    for devCardTier in gameState.tableState.devCards:
      for devCard in devCardTier:
        if devCard is None or devCard.prestige == 0: continue
        devCardList.append(devCard)
        devCardCostList.append(getCostInNumberOfGems(devCard.cost))
    devCardsAndCosts = zip(devCardCostList, devCardList)
    devCardOrderedList = [x for _, x in sorted(devCardsAndCosts, reverse=True)]
    for devCard in devCardOrderedList:
      # print "devCardOrdered=",devCard
      if gameRules.canBuyDevCard(gameState, self.index, devCard):
        # print "AffordableDevCard=",devCard
        action = getActionToBuy(devCard, legalMoves)
        break

    if action is None:
      action = getActionToTake3DiffGems(legalMoves)
    if action is None:
      action = getActionToBuyAnyAffordableCard(legalMoves)
    if action is None:
      action = getActionToReserveAnyCardIfPossible(legalMoves)
    if action is None: # NoOp
      action = [ord('0')+6]

    self.simKeyStrokes = action
    self.lastSimKeyStrokes = list(action)
    return action