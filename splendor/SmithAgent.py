from game import Agent

# Just do the first action in the list.
# Whenever possible, buy regardless of prestige points.
# Can't buy? Then get money
# Can't get money? Then reserve.
# Can't reserve? Then buy reserves.
# Can't? Do a NoOp.
class SmithAgent( Agent ):

  def __init__(self, index):
    self.index = index # agent/player index {0,1,2,3}
    self.isKeyboardAgent = False
    self.simKeyStrokes = []
    self.lastSimKeyStrokes = []

  def getAgentName(self):
    return "SmithAgent"

  def getAction(self, gameState):
    """
    To generate a actions, we need to define:
      + agentActions = gameState.getLegalActions(agentIndex)
      + succGameState = gameState.generateSuccessor(agentIndex, action)
      + gameState.isWin() # respect to agent 0
      + gameState.isLose() # respect to agent 0
    """
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions(self.index)
    if len(legalMoves) == 0:
      action = None
    else:
      action = legalMoves[0]

    # Choose one action at random
    self.simKeyStrokes = action
    self.lastSimKeyStrokes = list(action)
    return action