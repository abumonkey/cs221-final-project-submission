
  # Things a TableState should do:
  # - drawDevCardFromDeck(tier), returns devCard
  # - replaceDevCardAt(tier, rowIndex)
  #
  # - sellDevCard(position), returns devCard, update devCards, charge agent, update tokens
  # - sellDevCardReserve(), charge agent, update tokens
  # - grantDevCardReserve(position), returns devCard, update devCards, grant gold token, update tokens
  #   - if position = (tier, None) -> reserve from top of the deck.

  def drawDevCardFromDeck(self, tier):
    deck = self.decks[tier]
    assert len(deck) > 0, \
      "Cannot draw the next card. Deck tier {} is empty.".format(tier)
    # Updates deck and returns next card.
    return deckStack.pop(0)

  # When a development card is purchased/reserved...
  def replaceDevCardAt(self, tier, rowIndex):
    # validate there is a card in the location. --> Assert
    assert self.devCards[tier][rowIndex] != None, \
      "Cannot replace devCard in empty slot. (Tier={},rowIndex={}) is empty.".format(tier,rowIndex)
    # Remove the card from devCards
    devCard = self.devCards[tier][rowIndex]
    self.devCards[tier][rowIndex] = None
    # and replace it if deck[tier] is not empty
    if not self.decks[tier].isEmpty():
      self.devCards[tier][rowIndex] = drawDevCardFromDeck(tier)
    return devCard

  def nobleTileVisits(self, nobleTileIndex):
    # validate if available nobles. --> Assert
    assert len(self.nobleTiles) > 0, "No more nobleTiles available for visits."
    # remove from available nobleTiles list and
    nobleTile = self.nobleTiles[nobleTileIndex]
    self.nobleTiles.remove(nobleTile)
    # return noble tile who's visiting.
    return nobleTile