import random
from game import Agent

class RandomAgent( Agent ):

  def __init__(self, index):
    self.index = index # agent/player index {0,1,2,3}
    self.isKeyboardAgent = False
    self.simKeyStrokes = []
    self.lastSimKeyStrokes = []

  def getAgentName(self):
    return "RandomAgent"

  def getAction(self, gameState):
    """
    To generate a actions, we need to define:
      + agentActions = gameState.getLegalActions(agentIndex)
      + succGameState = gameState.generateSuccessor(agentIndex, action)
      + gameState.isWin() # respect to agent 0
      + gameState.isLose() # respect to agent 0
    """
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions(self.index)
    # print "len(legalMoves)=", len(legalMoves)
    if len(legalMoves) == 0:
      action =  None
    else:
      # Choose one action at random
      action = random.sample(legalMoves, 1)[0]

    self.simKeyStrokes = action
    self.lastSimKeyStrokes = list(action)
    return action