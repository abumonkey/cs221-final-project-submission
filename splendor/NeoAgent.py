from game import Agent, Actions
from splendorUtil import GemType
import msvcrt, sys

# Expecti-minimax Agent
class NeoAgent( Agent ):

  def __init__(self, index, depth):
    self.index = index # agent/player index {0,1,2,3}
    self.isKeyboardAgent = False
    self.simKeyStrokes = []
    self.lastSimKeyStrokes = []
    self.depth = depth
    self.numRecursions = 0

  def getAgentName(self):
    return "NeoAgent"

  def evaluationFunction(self, gameState):
    agentState = gameState.agentStates[self.index]
    prestige = agentState.getPrestige()

    numEmerald = agentState.tokens[GemType.index('Emerald')]
    numSapphire = agentState.tokens[GemType.index('Sapphire')]
    numRuby = agentState.tokens[GemType.index('Ruby')]
    numDiamond = agentState.tokens[GemType.index('Diamond')]
    numOnyx = agentState.tokens[GemType.index('Onyx')]

    balancedEconomy = 0
    if numEmerald > 0 and numSapphire > 0 and numRuby > 0 and numDiamond > 0 and numOnyx > 0:
      balancedEconomy = 1

    numJokers = agentState.jokers
    numNobleTiles = len(agentState.nobleTiles)
    numDevCards = len(agentState.devCards)

    value = 100*prestige +  \
            numEmerald +    \
            numSapphire +   \
            numRuby +       \
            numDiamond +    \
            numOnyx +       \
            10*balancedEconomy + \
            numJokers +     \
            300*numNobleTiles + \
            20*numDevCards

    # print "evalFunc=",value
    return value

  def getAction(self, gameState):

    # -----------------------------------------------------
    # Expecti-minimax Recursion
    # -----------------------------------------------------
    def recurseVexptminmax(gameState, depth, agentIndexInTurn, pendingChanceAction):
      self.numRecursions += 1
      if msvcrt.kbhit():
        print "self.numRecursions=",self.numRecursions
        sys.exit(0)

      # isEnd(s)
      if gameState.isEndState() and pendingChanceAction is None:
        return (gameState.utility(self.index), [ord('6')]) # NoOp

      # Max depth reached?
      if depth == 0 and pendingChanceAction is None:
        return (self.evaluationFunction(gameState), [ord('6')]) # NoOp

      # Is last agent/player in ply? Go one level deeper
      numAgents = len(gameState.agentStates)
      d = depth-1 if (agentIndexInTurn == (numAgents-1)) and (pendingChanceAction is None) else depth

      # Is Chance turn? -> Expected value
      if pendingChanceAction is not None:
        # Chance's turn!
        # action = [tier, column, index]
        # tier E{1,2,3}
        # Previous Agent actionType = reserved From Deck Top -> column is zero. {0}
        # Previous Agent actionType = reserved/bought Faced-up -> column is non-zero. {1,2,3,4}
        # index, one of many valid indices of the deck[tier-1]
        tier, column = pendingChanceAction
        numberOfChancePossibilities = len(gameState.tableState.decks[tier-ord('0')-1])
        print "Chance -> numberOfChancePossibilities", numberOfChancePossibilities
        prob = (1.0/numberOfChancePossibilities) if numberOfChancePossibilities else 0
        choices = []
        for index in range(numberOfChancePossibilities):
          action = [tier, column, index]
          succGameState = gameState.generateSuccessorWithChance(-1, action)
          # Append recursion. Also, wrap-around agentIndex to next player.
          # Clear pending chance action
          choices.append( (prob*recurseVexptminmax(succGameState, d, (agentIndexInTurn+1) % numAgents, None )[0], action) )
        # Return expectation according a uniform random model chance. Action is non-important here.
        return (sum(choice[0] for choice in choices), [ord('6')]) # NoOp

      # Is self's turn? -> Maximizer
      elif agentIndexInTurn == self.index:
        # Collect legal moves.
        agentActions = gameState.getLegalActions(agentIndexInTurn)
        print "Maximizer -> len(agentActions)", len(agentActions)
        choices = []
        for action in agentActions:
          succGameState = gameState.generateSuccessorWithChance(agentIndexInTurn, action)
          # Build pendingChanceAction only if actionType is reserve/buy.
          # if pendingChanceAction is built, then do not increment the agentIndexInTurn.
          actionType = action[0] - ord('0')
          builtPendingAction = None
          agentIndex = agentIndexInTurn
          if (actionType == Actions.BuyDevCard.value or actionType == Actions.ReserveDevCard.value):
            tier = action[1]
            column = action[2]
            if len(gameState.tableState.decks[tier - ord('0') - 1]) != 0:
              builtPendingAction = (tier, column)
            else:
              agentIndex += 1
          else:
            agentIndex += 1
          # Append recursion. Also, wrap-around agentIndex to next player.
          choices.append( (recurseVexptminmax(succGameState, d, agentIndex % numAgents, builtPendingAction )[0], action) )
        return max(choices)

      # Is opponent's turn? -> Minimizer
      elif 0 <= agentIndexInTurn < numAgents:
        # Collect legal moves.
        agentActions = gameState.getLegalActions(agentIndexInTurn)
        print "Minimizer -> len(agentActions)", len(agentActions)
        choices = []
        for action in agentActions:
          succGameState = gameState.generateSuccessorWithChance(agentIndexInTurn, action)
          # Build pendingChanceAction only if actionType is reserve/buy.
          # if pendingChanceAction is built, then do not increment the agentIndexInTurn.
          actionType = action[0] - ord('0')
          builtPendingAction = None
          agentIndex = agentIndexInTurn
          if actionType == Actions.BuyDevCard.value or actionType == Actions.ReserveDevCard.value:
            tier = action[1]
            column = action[2]
            if len(gameState.tableState.decks[tier - ord('0') - 1]) != 0:
              builtPendingAction = (tier, column)
            else:
              agentIndex += 1
          else:
            agentIndex += 1
          # Append recursion. Also, wrap-around agentIndex to next player.
          choices.append( (recurseVexptminmax(succGameState, d, agentIndex % numAgents, builtPendingAction )[0], action) )
        return min(choices)

      else:
        raise Exception("Unknown agentIndexInTurn: #{}".format(agentIndexInTurn))

    # -----------------------------------------------------
    # Minimax Recursion
    # -----------------------------------------------------
    def recurseVminmax(gameState, depth, agentIndexInTurn):
      self.numRecursions += 1
      # print "depth=",depth
      if msvcrt.kbhit():
        print "numRecursions=",self.numRecursions
        sys.exit(0)

      # isEnd(s)
      if gameState.isEndState():
        return (gameState.utility(self.index), [ord('6')]) # NoOp

      # Max depth reached?
      if depth == 0:
        # print "Value(gameState)=",self.evaluationFunction(gameState)
        return (self.evaluationFunction(gameState), [ord('6')]) # NoOp

      # Is last agent/player in ply? Go one level deeper
      numAgents = len(gameState.agentStates)
      d = depth-1 if (agentIndexInTurn == (numAgents-1)) else depth

      # Is self's turn? -> Maximizer
      if agentIndexInTurn == self.index:
        # Collect legal moves.
        agentActions = gameState.getLegalActions(agentIndexInTurn)
        # for action in agentActions:
        #   print "action=",action
        choices = []
        for i,action in enumerate(agentActions):
          # print "action:{} Maximizer - AgentIndex:{} - #Actions:{} - Depth:{}".format(i,agentIndexInTurn, len(agentActions), d)
          succGameState = gameState.generateSuccessorNoChance(agentIndexInTurn, action)
          # Append recursion. Also, wrap-around agentIndex to next player.
          choices.append( (recurseVminmax(succGameState, d, (agentIndexInTurn+1) % numAgents)[0], action) )
        # for choice in choices:
        #   print "choice=",choice
        return max(choices)

      # Is opponent's turn? -> Minimizer
      elif 0 <= agentIndexInTurn < numAgents:
        # Collect legal moves.
        agentActions = gameState.getLegalActions(agentIndexInTurn)
        choices = []
        for i,action in enumerate(agentActions):
          # print "action:{} Minimizer - AgentIndex:{} - #Actions:{} - Depth:{}".format(i,agentIndexInTurn, len(agentActions), d)
          succGameState = gameState.generateSuccessorNoChance(agentIndexInTurn, action)
          # Append recursion. Also, wrap-around agentIndex to next player.
          choices.append( (recurseVminmax(succGameState, d, (agentIndexInTurn+1) % numAgents)[0], action) )
        return min(choices)

      else:
        raise Exception("Unknown agentIndexInTurn: #{}".format(agentIndexInTurn))


    # -----------------------------------------------------
    # Minimax Recursion with Alpha-Beta Pruning
    # -----------------------------------------------------
    def recurseVminmaxPrune(gameState, depth, agentIndexInTurn, alpha, beta):
      self.numRecursions += 1
      # print "depth=",depth
      if msvcrt.kbhit():
        print "numRecursions=",self.numRecursions
        sys.exit(0)

      # isEnd(s)
      if gameState.isEndState():
        return (gameState.utility(self.index), [ord('6')]) # NoOp

      # Max depth reached?
      if depth == 0:
        # print "Value(gameState)=",self.evaluationFunction(gameState)
        return (self.evaluationFunction(gameState), [ord('6')]) # NoOp

      # Is last agent/player in ply? Go one level deeper
      numAgents = len(gameState.agentStates)
      d = depth-1 if (agentIndexInTurn == (numAgents-1)) else depth

      # Is self's turn? -> Maximizer
      if agentIndexInTurn == self.index:
        # Collect legal moves.
        agentActions = gameState.getLegalActions(agentIndexInTurn)
        localMax = (float('-inf'), [ord('6')]) # NoOp
        for i,action in enumerate(agentActions):
          # print "action:{} Maximizer - AgentIndex:{} - #Actions:{} - Depth:{}".format(i,agentIndexInTurn, len(agentActions), d)
          succGameState = gameState.generateSuccessorNoChance(agentIndexInTurn, action)
          # Maximize over recursion. Also, wrap-around agentIndex to next player.
          localMax = max(localMax,
                         (recurseVminmaxPrune(succGameState, d, (agentIndexInTurn+1) % numAgents, \
                                              alpha, beta)[0], action))
          alpha = max(alpha, localMax[0])
          if alpha >= beta:
            break # Prune!
        return localMax

      # Is opponent's turn? -> Minimizer
      elif 0 <= agentIndexInTurn < numAgents:
        # Collect legal moves.
        agentActions = gameState.getLegalActions(agentIndexInTurn)
        localMin = (float('+inf'), [ord('6')]) # NoOp
        for i,action in enumerate(agentActions):
          # print "action:{} Minimizer - AgentIndex:{} - #Actions:{} - Depth:{}".format(i,agentIndexInTurn, len(agentActions), d)
          succGameState = gameState.generateSuccessorNoChance(agentIndexInTurn, action)
          # Minimize over recursion. Also, wrap-around agentIndex to next player.
          localMin = min(localMin,
                         (recurseVminmaxPrune(succGameState, d, (agentIndexInTurn+1) % numAgents, \
                                              alpha, beta)[0], action))
          beta = min(beta, localMin[0])
          if alpha >= beta:
            break # Prune!
        return localMin

      else:
        raise Exception("Unknown agentIndexInTurn: #{}".format(agentIndexInTurn))


    # value, action = recurseVexptminmax(gameState, self.depth, self.index, None)
    # value, action = recurseVminmax(gameState, self.depth, self.index)
    # print "self.depth=",self.depth
    # Start alpha and beta with their worst possible score. (alpha=-inf, beta=+inf)
    value, action = recurseVminmaxPrune(gameState, self.depth, self.index, float('-inf'), float('inf'))
    # print 'NeoAgent says action = {}, value = {}'.format(action, value)
    # print "numRecursions=",self.numRecursions

    # Remember action picked
    self.simKeyStrokes = action
    self.lastSimKeyStrokes = list(action)
    return action
