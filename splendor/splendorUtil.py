import random
###################################################################################
# Splendor definitions

GemTypesColors = ['Green'  , 'Blue'    , 'Red' , 'White'  , 'Black']
GemType        = ['Emerald', 'Sapphire', 'Ruby', 'Diamond', 'Onyx' ]
JokerColor     = ['Yellow']
JokerType      = ['Gold'  ]

ActionStr = [ 'BuyDevCard',        \
              'Take2EqualGems',    \
              'Take3DiffGems',     \
              'ReserveDevCard',    \
              'BuyReservedDevCard',\
              'NoOp']

# Any noble awards the visited player with 3 prestige points.
# Each Noble Tile is a tuple of 2-3 requirements for the noble to visit.
# Each requirement is a tuple of 2 integers representing (gemType, number of gems of type gemType)
NoblePrestigePoints = 3
NobleTilesAll = [
    ( (GemTypesColors.index('Black'),4), (GemTypesColors.index('Red'  ),4) ) ,
    ( (GemTypesColors.index('Blue' ),4), (GemTypesColors.index('White'),4) ) ,
    ( (GemTypesColors.index('Black'),4), (GemTypesColors.index('White'),4) ) ,
    ( (GemTypesColors.index('Red'  ),4), (GemTypesColors.index('Green'),4) ) ,
    ( (GemTypesColors.index('Blue' ),4), (GemTypesColors.index('Green'),4) ) ,

    ( (GemTypesColors.index('Black'),3), (GemTypesColors.index('Blue' ),3), (GemTypesColors.index('White' ),3) ) ,
    ( (GemTypesColors.index('Green'),3), (GemTypesColors.index('Blue' ),3), (GemTypesColors.index('White' ),3) ) ,
    ( (GemTypesColors.index('Black'),3), (GemTypesColors.index('Red'  ),3), (GemTypesColors.index('Green' ),3) ) ,
    ( (GemTypesColors.index('Green'),3), (GemTypesColors.index('Blue' ),3), (GemTypesColors.index('Red'   ),3) ) ,
    ( (GemTypesColors.index('Black'),3), (GemTypesColors.index('Red'  ),3), (GemTypesColors.index('White' ),3) ) ,
    ]

# Define deck Level 1
Deck1Prestige = [0,0,0,0,0,0,0,1]
Deck1NumericCosts = [
    (1,2),
    (3,),
    (2,2),
    (1,1,1,1),
    (1,2,2),
    (1,1,3),
    (1,1,1,2),
    (4,)
]
Green1CostMap = [
    ( GemTypesColors.index('Blue' ), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Red'  ), ),
    ( GemTypesColors.index('Red'), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Blue'), GemTypesColors.index('Red'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Red'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('White'), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Blue'), GemTypesColors.index('Red'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Black'), )
]
Blue1CostMap = [
    ( GemTypesColors.index('White' ), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Black'  ), ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Green'), GemTypesColors.index('Red'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Red'), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Red'), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Green'), GemTypesColors.index('Black'), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('Red'), )
]
Red1CostMap = [
    ( GemTypesColors.index('Green' ), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('White'  ), ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Blue'), GemTypesColors.index('Green'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('White'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Red'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Green'), GemTypesColors.index('Blue'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('White'), )
]
White1CostMap = [
    ( GemTypesColors.index('Black' ), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('Blue'  ), ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Green'), GemTypesColors.index('Red'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Green'), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Blue'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Red'), GemTypesColors.index('Black'), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('Green'), )
]
Black1CostMap = [
    ( GemTypesColors.index('Red' ), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('Green'  ), ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Green'), GemTypesColors.index('Red'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Red'), GemTypesColors.index('Blue'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Green'), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Red'), GemTypesColors.index('Green'), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('Blue'), )
]

# Define deck Level 2
Deck2Prestige = [1,1,2,2,2,3]
Deck2NumericCosts = [
    (2,2,3),
    (2,3,3),
    (5,),
    (3,5),
    (1,2,4),
    (6,)
]
Green2CostMap     = [
    ( GemTypesColors.index('Black'), GemTypesColors.index('White'  ), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('White'  ), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('Green' ), ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Blue'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Green'), )
]
Blue2CostMap = [
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Green'  ), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Green'  ), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Blue' ), ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Red'), GemTypesColors.index('White'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Blue'), )
]
Red2CostMap = [
    ( GemTypesColors.index('White'), GemTypesColors.index('Red'  ), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Red'), GemTypesColors.index('Blue'  ), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('Black' ), ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Green'), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('Red'), )
]
White2CostMap = [
    ( GemTypesColors.index('Red'), GemTypesColors.index('Black'  ), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Blue'  ), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('Red' ), ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('Black'), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('White'), )
]
Black2CostMap = [
    ( GemTypesColors.index('Green'), GemTypesColors.index('Blue'  ), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Green'  ), GemTypesColors.index('White') ),
    ( GemTypesColors.index('White' ), ),
    ( GemTypesColors.index('Red'), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('Red'), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('Black'), )
]

# Define deck Level 3
Deck3Prestige = [3,4,4,5]
Deck3NumericCosts = [
    (3,3,3,5),
    (7,),
    (3,3,6),
    (3,7)
]
Green3CostMap     = [
    ( GemTypesColors.index('Black'), GemTypesColors.index('Red'  ), GemTypesColors.index('Blue'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Blue' ), ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Green'), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('Blue' ) )
]
Blue3CostMap     = [
    ( GemTypesColors.index('White'), GemTypesColors.index('Green'  ), GemTypesColors.index('Red'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('White' ), ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Blue'), GemTypesColors.index('White') ),
    ( GemTypesColors.index('Blue'), GemTypesColors.index('White' ) )
]
Red3CostMap     = [
    ( GemTypesColors.index('Black'), GemTypesColors.index('Green'  ), GemTypesColors.index('White'), GemTypesColors.index('Blue') ),
    ( GemTypesColors.index('Green' ), ),
    ( GemTypesColors.index('Red'), GemTypesColors.index('Blue'), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('Red'), GemTypesColors.index('Green' ) )
]
White3CostMap     = [
    ( GemTypesColors.index('Black'), GemTypesColors.index('Green'  ), GemTypesColors.index('Blue'), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('Black' ), ),
    ( GemTypesColors.index('Red'), GemTypesColors.index('White'), GemTypesColors.index('Black') ),
    ( GemTypesColors.index('White'), GemTypesColors.index('Black' ) )
]
Black3CostMap     = [
    ( GemTypesColors.index('White'), GemTypesColors.index('Blue'  ), GemTypesColors.index('Red'), GemTypesColors.index('Green') ),
    ( GemTypesColors.index('Red' ), ),
    ( GemTypesColors.index('Green'), GemTypesColors.index('Black'), GemTypesColors.index('Red') ),
    ( GemTypesColors.index('Black'), GemTypesColors.index('Red' ) )
]

# Deck Aggregates
DeckLevelPrestigeList = [Deck1Prestige, Deck2Prestige, Deck3Prestige]
DeckLevelNumericCostsList = [Deck1NumericCosts, Deck2NumericCosts, Deck3NumericCosts]
GreenLevelCostMapList = [Green1CostMap, Green2CostMap, Green3CostMap]
BlueLevelCostMapList = [Blue1CostMap, Blue2CostMap, Blue3CostMap]
RedLevelCostMapList = [Red1CostMap, Red2CostMap, Red3CostMap]
WhiteLevelCostMapList = [White1CostMap, White2CostMap, White3CostMap]
BlackLevelCostMapList = [Black1CostMap, Black2CostMap, Black3CostMap]

StartDeckLevel1 = 0     # 40 total
StartDeckLevel2 = 40    # 30 total
StartDeckLevel3 = 70    # 20 total

###################################################################################
# Splendor utilities

############################################################
# Generate Deck:
def generateDeck():

    def generateDeckLevel(level):
        levelDeck = []
        deckPrestige = DeckLevelPrestigeList[level - 1]
        deckNumericCosts = DeckLevelNumericCostsList[level - 1]
        greenCostMap = GreenLevelCostMapList[level - 1]
        blueCostMap = BlueLevelCostMapList[level - 1]
        redCostMap = RedLevelCostMapList[level - 1]
        whiteCostMap = WhiteLevelCostMapList[level - 1]
        blackCostMap = BlackLevelCostMapList[level - 1]
        for gemTypeIndex in range(len(GemTypesColors)):
            for i in range(len(deckPrestige)):
                prestige = deckPrestige[i]
                numericCost = deckNumericCosts[i]
                # cost is a tuple of 1-4 gemCosts.
                # Each gemCost is a 2 element tuple: (gemType, number of gems of type gemType)
                if GemTypesColors.index('Green') == gemTypeIndex:
                    costMap = greenCostMap
                elif GemTypesColors.index('Blue') == gemTypeIndex:
                    costMap = blueCostMap
                elif GemTypesColors.index('Red') == gemTypeIndex:
                    costMap = redCostMap
                elif GemTypesColors.index('White') == gemTypeIndex:
                    costMap = whiteCostMap
                elif GemTypesColors.index('Black') == gemTypeIndex:
                    costMap = blackCostMap
                gemTypeCost = costMap[i]
                cost = ( (gemTypeCost[i], numericCost[i]) for i in range(len(numericCost)) )
                card = (level, gemTypeIndex, prestige, tuple(cost))
                levelDeck.append(card)

        return levelDeck

    deck = []
    for level in range(3):
        subDeck = generateDeckLevel(level+1)
        deck.append(subDeck)
    return deck

############################################################

def setupTableCards(tableDeck):
    tableDeck1Indexes = [ i for i in range(StartDeckLevel1, StartDeckLevel2) ]
    tableDeck2Indexes = [ i for i in range(StartDeckLevel2, StartDeckLevel3) ]
    tableDeck3Indexes = [ i for i in range(StartDeckLevel3, len(tableDeck)) ]
    tableCardsLevel1 = random.sample(tableDeck1Indexes, 4)
    tableCardsLevel2 = random.sample(tableDeck2Indexes, 4)
    tableCardsLevel3 = random.sample(tableDeck3Indexes, 4)
    tableCards = tableCardsLevel1 + tableCardsLevel2 + tableCardsLevel3
    # Remove cards selected from the tableDeck list
    for card in tableCards:
        tableDeck[card] = 0
    tableCards = []
    tableCards.append(tuple(tableCardsLevel1))
    tableCards.append(tuple(tableCardsLevel2))
    tableCards.append(tuple(tableCardsLevel3))
    return tuple(tableCards)

############################################################

def checkValidNumberOfPlayers(numPlayers):
    if not (2 <= numPlayers <= 4):
        raise Exception("Game Rule violation: Number of players must be 2-4")

############################################################

def setupTableNobles(numPlayers):
    checkValidNumberOfPlayers(numPlayers)
    numberOfNoblesToDraw = numPlayers+1
    nobleTilesIndexesAll = [ i for i in range(len(NobleTilesAll)) ]
    tableNobles = random.sample(nobleTilesIndexesAll, numberOfNoblesToDraw)
    return tuple(tableNobles)

############################################################

def getGemColorByType(gemType):
    return GemTypesColors[ GemType.index(gemType) ]

############################################################

def getTokenMultiplicity(numPlayers):
    checkValidNumberOfPlayers(numPlayers)
    if numPlayers == 4: return 7
    if numPlayers == 3: return 5
    if numPlayers == 2: return 4