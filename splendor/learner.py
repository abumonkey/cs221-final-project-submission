from pprint import pprint
import cPickle
import os, subprocess
import multiprocessing
from multiprocessing import Process
import collections
import util
from game import GameRules

from splendorUtil import GemType

def runSplendor(cmd):
  os.system(cmd)

def numberOfDevCards(gemType, devCards):
  acum = 0
  for devCard in devCards:
    if devCard is None: continue
    if devCard.gemType == gemType:
      acum += 1
  return acum

def canAffordDevCardWithPoints(gs):
  gameRules = GameRules()
  for devCardTier in gs.tableState.devCards:
    for devCard in devCardTier:
      if devCard is None: continue
      if (devCard.prestige != 0) and gameRules.canBuyDevCard(gs, 0, devCard):
        return True
  return False

def canAffordDevCardWithoutPoints(gs):
  gameRules = GameRules()
  for devCardTier in gs.tableState.devCards:
    for devCard in devCardTier:
      if devCard is None: continue
      if (devCard.prestige == 0) and gameRules.canBuyDevCard(gs, 0, devCard):
        return True
  return False

def canAffordDevCardinNobles(gs):

  def devCardInNobles(devCard):
    for nobleTile in gs.tableState.nobleTiles:
      nobleGemCostList = nobleTile.requirements.gemCostTuplesList
      for gemCost in nobleGemCostList:
        # print "devCard.gemType=", devCard.gemType
        # print "gemCost[1]=", gemCost[1]
        if devCard.gemType == gemCost[1]:
          return True
    return False

  gameRules = GameRules()
  for devCardTier in gs.tableState.devCards:
    for devCard in devCardTier:
      if devCard is None: continue
      if devCardInNobles(devCard) and gameRules.canBuyDevCard(gs, 0, devCard):
        return True
  return False

def getCostDistToCloserNoble(gs, agentIndex):
  nobles = gs.tableState.nobleTiles
  devCards = gs.agentStates[agentIndex].devCards
  if len(nobles) == 0: return 0 # this causes the feature to be ignored.

  # Get min cost distance to nobles.
  costDist = float('inf')
  for noble in nobles:
    nobleCost = noble.requirements.gemCostTuplesList
    distance = 0
    for gemCostTuple in nobleCost:
      gemType = gemCostTuple[1]
      diff = gemCostTuple[0] - numberOfDevCards(gemType, devCards)
      if diff > 0:
        distance = distance + diff
    costDist = min(costDist, distance)
  return costDist

def compareCosts(cost1, cost2):
  # return True if cost1 < cost2 else False
  # Comparisson is done by number of total gems.
  numGems1 = sum([gemCost[0] for gemCost in cost1.gemCostTuplesList])
  numGems2 = sum([gemCost[0] for gemCost in cost2.gemCostTuplesList])
  return numGems1 < numGems2

def getCostDistToCard(devCard, gs, agentIndex):
  gameRules = GameRules()
  isItForFree = gameRules.isItForFree(gs, devCard.cost, agentIndex)
  if isItForFree:
    return 0
  else:
    agentState = gs.agentStates[agentIndex]
    distance = 0
    gemCostList = list(devCard.cost.gemCostTuplesList)
    for gemCost in gemCostList:
      gemDevCardCount = sum([1 if (gemCost[1] == devCard.gemType) else 0 for devCard in agentState.devCards])
      diff = gemCost[0] - gemDevCardCount
      if diff <= 0: continue # this gemType is free.
      distance += diff
    return distance


def featureExtractor(gs):
  phi_gs = collections.defaultdict(float)

  # phi_gs['gameProgress'] = gs.getProgress()

  table = gs.tableState
  # Adding table state features
  # for i in range(table.tokenMultiplicity):
  #   if (table.tokens[GemType.index('Emerald')] == (i+1)):
  #     phi_gs['bankEmerald_eq_{}'.format(i+1)] = 1.
  #   if (table.tokens[GemType.index('Sapphire')] == (i+1)):
  #     phi_gs['bankSapphire_eq_{}'.format(i+1)] = 1.
  #   if (table.tokens[GemType.index('Ruby')] == (i+1)):
  #     phi_gs['bankRuby_eq_{}'.format(i+1)] = 1.
  #   if (table.tokens[GemType.index('Diamond')] == (i+1)):
  #     phi_gs['bankDiamond_eq_{}'.format(i+1)] = 1.
  #   if (table.tokens[GemType.index('Onyx')] == (i+1)):
  #     phi_gs['bankOnyx_eq_{}'.format(i+1)] = 1.

  # for i in range(table.jokerMultiplicity):
  #   if (table.jokers == (i+1)):
  #     phi_gs['bankJoker_eq_{}'.format(i+1)] = 1.

  # for i in range(table.nobleMultiplicity):
  #   if len(table.nobleTiles) == (i+1):
  #     phi_gs['bankNoble_eq_{}'.format(i+1)] = 1.

  # Track up to 3 devCard rows/tiers.
  # Track up to 4 gems of same type in a single row.
  # for i in range(3):
  #   for j in range(4):
  #     if numberOfDevCards('Emerald', table.devCards[i]) == (j+1):
  #       phi_gs['bankCardEmerald_tier{}_eq_{}'.format(i+1,j+1)] = 1.
  #     if numberOfDevCards('Sapphire', table.devCards[i]) == (j+1):
  #       phi_gs['bankCardSapphire_tier{}_eq_{}'.format(i+1,j+1)] = 1.
  #     if numberOfDevCards('Ruby', table.devCards[i]) == (j+1):
  #       phi_gs['bankCardRuby_tier{}_eq_{}'.format(i+1,j+1)] = 1.
  #     if numberOfDevCards('Diamond', table.devCards[i]) == (j+1):
  #       phi_gs['bankCardDiamond_tier{}_eq_{}'.format(i+1,j+1)] = 1.
  #     if numberOfDevCards('Onyx', table.devCards[i]) == (j+1):
  #       phi_gs['bankCardOnyx_tier{}_eq_{}'.format(i+1,j+1)] = 1.

  # Number of remaining deck[tier] devCards.
  # for i in range(3):
  #   phi_gs['bankDeck_tier{}_numLeft'.format(i+1)] = len(table.decks[i])


  # Adding all agents state features
  # for k,agentState in enumerate(gs.agentStates):
  #   for i in range(table.tokenMultiplicity):
  #     if (agentState.tokens[GemType.index('Emerald')] == (i+1)):
  #       phi_gs['agent{}_Emerald_eq_{}'.format(k,i+1)] = 1.
  #     if (agentState.tokens[GemType.index('Sapphire')] == (i+1)):
  #       phi_gs['agent{}_Sapphire_eq_{}'.format(k,i+1)] = 1.
  #     if (agentState.tokens[GemType.index('Ruby')] == (i+1)):
  #       phi_gs['agent{}_Ruby_eq_{}'.format(k,i+1)] = 1.
  #     if (agentState.tokens[GemType.index('Diamond')] == (i+1)):
  #       phi_gs['agent{}_Diamond_eq_{}'.format(k,i+1)] = 1.
  #     if (agentState.tokens[GemType.index('Onyx')] == (i+1)):
  #       phi_gs['agent{}_Onyx_eq_{}'.format(k,i+1)] = 1.

    # for i in range(table.jokerMultiplicity):
    #   if (agentState.jokers == (i+1)):
    #     phi_gs['agent{}_Joker_eq_{}'.format(k,i+1)] = 1.

    # prestige = agentState.getPrestige()
    # if prestige <= 5:
    #   phi_gs['agent{}_Prestige_lte_{}'.format(k,5)] = 1.
    # if prestige <= 10:
    #   phi_gs['agent{}_Prestige_lte_{}'.format(k,10)] = 1.
    # if prestige == 11:
    #   phi_gs['agent{}_Prestige_eq_{}'.format(k,11)] = 1.
    # if prestige == 12:
    #   phi_gs['agent{}_Prestige_eq_{}'.format(k,12)] = 1.
    # if prestige == 13:
    #   phi_gs['agent{}_Prestige_eq_{}'.format(k,13)] = 1.
    # if prestige == 14:
    #   phi_gs['agent{}_Prestige_eq_{}'.format(k,14)] = 1.
    # if prestige >= 15:
    #   phi_gs['agent{}_Prestige_gte_{}'.format(k,15)] = 1.
    # if prestige == 16:
    #   phi_gs['agent{}_Prestige_eq_{}'.format(k,16)] = 1.
    # if prestige == 17:
    #   phi_gs['agent{}_Prestige_eq_{}'.format(k,17)] = 1.

    # for i in range(table.nobleMultiplicity):
    #   if len(agentState.nobleTiles) == (i+1):
    #     phi_gs['agent{}_Noble_eq_{}'.format(k,i+1)] = 1.

    # Track up to 7 devCards per gemType
    # for i in range(7):
    #   if (numberOfDevCards('Emerald', agentState.devCards) == (i+1)):
    #     phi_gs['agent{}_cardEmerald_eq_{}'.format(k,i+1)] = 1.
    #   if (numberOfDevCards('Sapphire', agentState.devCards) == (i+1)):
    #     phi_gs['agent{}_cardSapphire_eq_{}'.format(k,i+1)] = 1.
    #   if (numberOfDevCards('Ruby', agentState.devCards) == (i+1)):
    #     phi_gs['agent{}_cardRuby_eq_{}'.format(k,i+1)] = 1.
    #   if (numberOfDevCards('Diamond', agentState.devCards) == (i+1)):
    #     phi_gs['agent{}_cardDiamond_eq_{}'.format(k,i+1)] = 1.
    #   if (numberOfDevCards('Onyx', agentState.devCards) == (i+1)):
    #     phi_gs['agent{}_cardOnyx_eq_{}'.format(k,i+1)] = 1.

    # Track if greater than or equal to 8 devCards per gemType
    # if (numberOfDevCards('Emerald', agentState.devCards) >= 8):
    #   phi_gs['agent{}_cardEmerald_gte_{}'.format(k,8)] = 1.
    # if (numberOfDevCards('Sapphire', agentState.devCards) >= 8):
    #   phi_gs['agent{}_cardSapphire_gte_{}'.format(k,8)] = 1.
    # if (numberOfDevCards('Ruby', agentState.devCards) >= 8):
    #   phi_gs['agent{}_cardRuby_gte_{}'.format(k,8)] = 1.
    # if (numberOfDevCards('Diamond', agentState.devCards) >= 8):
    #   phi_gs['agent{}_cardDiamond_gte_{}'.format(k,8)] = 1.
    # if (numberOfDevCards('Onyx', agentState.devCards) >= 8):
    #   phi_gs['agent{}_cardOnyx_gte_{}'.format(k,8)] = 1.

    # Track up to 3 reserve devCards per gemType
    # for i in range(3):
    #   if (numberOfDevCards('Emerald', agentState.devCardReserves) == (i+1)):
    #     phi_gs['agent{}_resEmerald_eq_{}'.format(k,i+1)] = 1.
    #   if (numberOfDevCards('Sapphire', agentState.devCardReserves) == (i+1)):
    #     phi_gs['agent{}_resSapphire_eq_{}'.format(k,i+1)] = 1.
    #   if (numberOfDevCards('Ruby', agentState.devCardReserves) == (i+1)):
    #     phi_gs['agent{}_resRuby_eq_{}'.format(k,i+1)] = 1.
    #   if (numberOfDevCards('Diamond', agentState.devCardReserves) == (i+1)):
    #     phi_gs['agent{}_resDiamond_eq_{}'.format(k,i+1)] = 1.
    #   if (numberOfDevCards('Onyx', agentState.devCardReserves) == (i+1)):
    #     phi_gs['agent{}_resOnyx_eq_{}'.format(k,i+1)] = 1.

  ###################################################################
  # Feature Set #2
  ###################################################################
  # can Agent0 afford devCard with points?
  if canAffordDevCardWithPoints(gs):
    phi_gs['canAffordDevCardWithPoints'] = 1.
  # can Agent0 afford devCard without points?
  elif canAffordDevCardWithoutPoints(gs):
    phi_gs['canAffordDevCardWithoutPoints'] = 1.
  else:
    phi_gs['cannotAffordDevCard'] = 1.

  # can Agent0 afford devCard with gemType present in Noble Tiles?
  if canAffordDevCardinNobles(gs):
    phi_gs['canAffordDevCardinNobles'] = 1.

  # Prestige compare
  agentPrestige = gs.agentStates[0].getPrestige()
  oppPrestige = gs.agentStates[1].getPrestige()
  diffPrestige = agentPrestige - oppPrestige
  if diffPrestige > 0:
    phi_gs['agent0_MorePrestige'] = 1.
  if diffPrestige < 0:
    phi_gs['agent0_LessPrestige'] = 1.

  # Noble compare
  agentNumNobleTiles = len(gs.agentStates[0].nobleTiles)
  oppNumNobleTiles = len(gs.agentStates[1].nobleTiles)
  diffNumNobleTiles = agentNumNobleTiles - oppNumNobleTiles
  if diffNumNobleTiles > 0:
    phi_gs['agent0_MoreNobleTiles'] = 1.
  if diffNumNobleTiles < 0:
    phi_gs['agent0_LessNobleTiles'] = 1.

  # DevCards compare
  agentNumDevCards = len(gs.agentStates[0].devCards)
  oppNumDevCards = len(gs.agentStates[1].devCards)
  diffNumDevCards = agentNumDevCards - oppNumDevCards
  if diffNumDevCards > 0:
    phi_gs['agent0_MoreDevCards'] = 1.
  if diffNumDevCards < 0:
    phi_gs['agent0_LessDevCards'] = 1.

  ###################################################################

  ###################################################################
  # Feature Set #3
  ###################################################################
  # Cost Distance to Next Noble compare
  agentCostDistToCloserNoble = getCostDistToCloserNoble(gs, agentIndex=0)
  oppCostDistToCloserNoble = getCostDistToCloserNoble(gs, agentIndex=1)
  diffCostDistToCloserNoble = agentCostDistToCloserNoble - oppCostDistToCloserNoble
  if diffCostDistToCloserNoble > 0:
    phi_gs['agent0_FartherToNoble'] = 1.
  if diffCostDistToCloserNoble < 0:
    phi_gs['agent0_CloserToNoble'] = 1.

  # Cost Distance to Cheapest Card compare
  # Get cheapest with prestige
  # Init with tier 3, column 0 devCard. All tier 3 devCards have prestige > 0.
  cheaspestDevCard = gs.tableState.devCards[2][0]
  for devCardTier in gs.tableState.devCards:
    for devCard in devCardTier:
      if devCard is None:
        continue
      if cheaspestDevCard is None:
        cheaspestDevCard = devCard
        continue
      if compareCosts(devCard.cost, cheaspestDevCard.cost):
        cheaspestDevCard = devCard

  if cheaspestDevCard is not None:
    agentCostDistToCheapestCard = getCostDistToCard(cheaspestDevCard, gs, agentIndex=0)
    oppCostDistToCheapestCard = getCostDistToCard(cheaspestDevCard, gs, agentIndex=1)
    diffCostDistToCheapestCard = agentCostDistToCheapestCard - oppCostDistToCheapestCard
    if diffCostDistToCheapestCard > 0:
      phi_gs['agent0_FartherToCheapestCard'] = 1.
    if diffCostDistToCheapestCard < 0:
      phi_gs['agent0_CloserToCheapestCard'] = 1.

  # Cost Distance to Cheapest Card with Points compare
  cheaspestDevCardPoints = gs.tableState.devCards[2][0]
  for devCardTier in gs.tableState.devCards:
    for devCard in devCardTier:
      if devCard is None or devCard.prestige == 0:
        continue
      if cheaspestDevCardPoints is None:
        cheaspestDevCardPoints = devCard
        continue
      if compareCosts(devCard.cost, cheaspestDevCardPoints.cost):
        cheaspestDevCardPoints = devCard

  if cheaspestDevCardPoints is not None:
    agentCostDistToCheapestCardPoints = getCostDistToCard(cheaspestDevCardPoints, gs, agentIndex=0)
    oppCostDistToCheapestCardPoints = getCostDistToCard(cheaspestDevCardPoints, gs, agentIndex=1)
    diffCostDistToCheapestCardPoints = agentCostDistToCheapestCardPoints - oppCostDistToCheapestCardPoints
    if diffCostDistToCheapestCardPoints > 0:
      phi_gs['agent0_FartherToCheapestCardPoints'] = 1.
    if diffCostDistToCheapestCardPoints < 0:
      phi_gs['agent0_CloserToCheapestCardPoints'] = 1.

  # numEmerald = agentState.tokens[GemType.index('Emerald')]
  # numSapphire = agentState.tokens[GemType.index('Sapphire')]
  # numRuby = agentState.tokens[GemType.index('Ruby')]
  # numDiamond = agentState.tokens[GemType.index('Diamond')]
  # numOnyx = agentState.tokens[GemType.index('Onyx')]

  # balancedEconomy = 0
  # if numEmerald > 0 and numSapphire > 0 and numRuby > 0 and numDiamond > 0 and numOnyx > 0:
  #   balancedEconomy = 1

  # numJokers = agentState.jokers
  # numNobleTiles = len(agentState.nobleTiles)
  # numDevCards = len(agentState.devCards)

  # value = 100*prestige +  \
  #         numEmerald +    \
  #         numSapphire +   \
  #         numRuby +       \
  #         numDiamond +    \
  #         numOnyx +       \
  #         10*balancedEconomy + \
  #         numJokers +     \
  #         300*numNobleTiles + \
  #         20*numDevCards

  # phi_gs['prestige'] = prestige
  # phi_gs['numEmerald'] = numEmerald
  # phi_gs['numSapphire'] = numSapphire
  # phi_gs['numRuby'] = numRuby
  # phi_gs['numDiamond'] = numDiamond
  # phi_gs['numOnyx'] = numOnyx
  # if balancedEconomy:
  #   phi_gs['balancedEconomy'] = .1
  # phi_gs['numJokers'] = numJokers
  # phi_gs['numNobleTiles'] = numNobleTiles
  # phi_gs['numDevCards'] = numDevCards

  return phi_gs

def runTDLearning(weightVector, episode, eta):
  numElements = len(episode)
  if (numElements-1) % 3 != 0:
    raise Exception("this episode is incomplete!")

  episodeErr = 0.0
  for i,element in enumerate(episode):
    # If not a gameState, continue to next element.
    if i % 3 != 0: continue
    # If this is the endState, nothing else to do.
    if i == (numElements-1): continue

    # This element is a gameState.
    currentGS = element
    action = episode[i+1]
    reward = episode[i+2]
    nextGS = episode[i+3]

    # On each (s,a,r,s'):
    phi_currentGS = featureExtractor(currentGS)
    phi_nextGS = featureExtractor(nextGS)
    Value_currentGS = util.dotProduct(weightVector, phi_currentGS)
    Value_nextGS = util.dotProduct(weightVector, phi_nextGS)

    # Assuming discount factor = 1 since this is a game!
    error = Value_currentGS - (reward + 0.9*Value_nextGS)
    episodeErr += (error * error)
    gradient = collections.defaultdict(float)
    util.increment(gradient, error, phi_currentGS)

    # print "-- GRADIENT ---------------------------------------------------"
    # for key in sorted(gradient.keys()):
    #   print "w[{:30}]={:18}".format(key,gradient[key])

    # print "-- BEFORE ---------------------------------------------------"
    # for key in sorted(weightVector.keys()):
    #   print "w[{:30}]={:18}".format(key,weightVector[key])

    util.increment(weightVector, (-1)*eta, gradient)

    # print "-- AFTER ----------------------------------------------------"
    # for key in sorted(weightVector.keys()):
    #   print "w[{:30}]={:18}".format(key,weightVector[key])

  Value_endGS = util.dotProduct(weightVector, featureExtractor(episode[-1]))
  episodeErr = episodeErr / ((numElements-1) / 3)
  print "Utility = {:_<4}, Value = {:15}, Error = {:15}".format(episode[-2], Value_endGS, episodeErr)
  # print "Utility = {:_<4}, Value(endState) = {:18}".format(episode[-2], Value_endGS)


# ===============================================================================
def learnSplendor():
  # This can be read from a file to leverage previously learned experience!
  filename = 'weights'
  if os.path.isfile(filename):
    f = open(filename)
    try: data = cPickle.load(f)
    finally: f.close()
    weightVector = data['weights']
  # No previous experience. Load all Zeros.
  else:
    weightVector = collections.defaultdict(float)

  # Clean-up data directory.
  for filename in os.listdir('./genTrainData'):
    os.remove('./genTrainData/'+filename)

  # print "-------------------------------------------"
  # print "Generating training data from self-play ..."
  cpu_count = multiprocessing.cpu_count()
  # print "Workers: ", cpu_count

  # Launch n games per processor.
  numGames = 1
  parallel = True # dont forget to use '-q' for parallel.
  # myCmd = "python splendor.py -l -q -r -k 1 -n {} -d 1".format(numGames)
  myCmd = "python splendor.py -l -q -r -n {} -d 1".format(numGames)
  # myCmd = "python splendor.py -k 1 -n {} -d 1".format(numGames)
  if parallel:
    procs = []
    for i in range(cpu_count):
      proc = Process(target=runSplendor, args=(myCmd,))
      procs.append(proc)
      proc.start()
    for proc in procs:
      proc.join()
  else:
    for i in range(1):
      runSplendor(myCmd)

  # print "Self-play finished."

  # Read episodes
  # print "Updating parametrized V(gameState;w) ..."
  for filename in os.listdir('./genTrainData'):
    # print filename
    f = open('./genTrainData/'+filename)
    try: recorded = cPickle.load(f)
    finally: f.close()
    episode = recorded['episode']
    for learnIter in range(1):
      print "Episode Learning Iter:", learnIter
      runTDLearning(weightVector, episode, 0.01)

  for key in sorted(weightVector.keys()):
    print "w[{:30}]={:18}".format(key,weightVector[key])
  # Log to file weights
  fname = 'weights'
  f = file(fname, 'w')
  components = {'weights': weightVector}
  cPickle.dump(components, f)
  f.close()

  # raw_input()


if __name__ == '__main__':
  for i in range(1000):
    print "iteration=", i
    learnSplendor()
  pass