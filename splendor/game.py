from util import *
from util import raiseNotDefined
from enum import Enum
from splendorUtil import GemTypesColors, GemType, JokerType, ActionStr
import time, os
import traceback
import splendorUtil
import curses
from collections import Counter
import copy


class Agent:
  """
  An agent must define a getAction method, but may also define the
  following methods which will be called if they exist:

  def registerInitialState(self, state): # inspects the starting state
  """
  # Init with player/agent/merchant index number.
  def __init__(self, index=0):
    self.index = index

  def getAction(self, state):
    """
    The Agent will receive a GameState and must return an action from
    Actions.{ Take3DiffGems, Take2EqualGems, ReserveDevCard,
              BuyDevCard, BuyReservedDevCard}
    """
    raiseNotDefined()

  def getAgentName(self):
    raiseNotDefined()


class Actions(Enum):
  """
  The legal actions depend on the current state of the game.
  A GameState will call state.getLegalActions( agentIndex )
  GameState is defined in splendor.py
  """
  BuyDevCard = 1
  Take2EqualGems = 2
  Take3DiffGems = 3
  ReserveDevCard = 4
  BuyReservedDevCard = 5
  NoOp = 6
  DiscardTokens = 7


class Cost:
  """
  A Cost in splendor is a list of Gem costs.
  Each Gem cost is a tuple of (numberOfGems, gemType)
      e.g. ( 3, Emeralds )
  """
  def __init__(self, gemCostTuplesList):
    self.gemCostTuplesList = gemCostTuplesList

  def getGemCostTuplesList(self):
    return self.gemCostTuplesList

  def __eq__(self, other):
    if other == None: return False
    if len(self.gemCostTuplesList) != len(other.gemCostTuplesList): return False
    for i in range(len(self.gemCostTuplesList)):
      if (self.gemCostTuplesList[i][0] != other.gemCostTuplesList[i][0]) or \
         (self.gemCostTuplesList[i][1] != other.gemCostTuplesList[i][1]):
        return False
    return True

  def __hash__(self):
    retValToHash = tuple(self.gemCostTuplesList)
    return hash(retValToHash)

  def __str__(self):
    cost = [ (gemCost[0], splendorUtil.getGemColorByType(gemCost[1]),
              gemCost[1]) for gemCost in self.gemCostTuplesList       ]
    return str(cost)


class DevelopmentCard:
  """
  A DevelopmentCard is represented by tier, gem type, prestige, cost
  """
  def __init__(self, tier, gemType, prestige, cost):
    self.tier = tier
    self.gemType = gemType
    self.prestige = prestige
    self.cost = cost

  def getTier(self):
    return self.tier

  def getGemType(self):
    return self.gemType

  def getGemColor(self):
    return splendorUtil.getGemColorByType(self.gemType)

  def getPrestige(self):
    return self.prestige

  def getCost(self):
    return self.cost

  def __eq__(self, other):
    if other == None: return False
    return (self.tier == other.tier and self.gemType == other.gemType and \
            self.prestige == other.prestige and self.cost == other.cost)

  def __hash__(self):
    retValToHash = (self.tier, self.gemType,
                    self.prestige, hash(self.cost))
    return hash(retValToHash)

  def __str__(self):
    return "Tier(GemColor, Gem, Prestige) : " + \
           "{}({}, {}, {})".format(self.tier, self.getGemColor(),
                                   self.gemType, self.prestige)

  def displayDevCard(self):
    print "======== Dev Card (Tier {}) ========".format(self.tier)
    print " Gem Color = ", self.getGemColor()
    print " Gem = ", self.gemType
    print " Prestige = ", self.prestige
    print " Cost = ", str( self.cost )
    print "------------------------------------"


class NobleTile:
  """
  A NobleTile is represented by prestige and requirements.
  In Splendor, a noble tile requirement is a list of Gem requirements.
  Each Gem requirement is a tuple of (numberOfGems, GemColorIndex, GemTypeIndex)
      e.g. ( 3, Green, Emeralds )
      Requirement is really a cost object but with a different name - since
      getting a noble tile does not cost anything.
  """
  def __init__(self, prestige, requirements):
    self.prestige = prestige
    self.requirements = requirements

  def getPrestige(self):
    return self.prestige

  def getRequirements(self):
    return self.requirements

  def __hash__(self):
    retValToHash = (self.prestige, hash(self.requirements))
    return hash(retValToHash)

  def __str__(self):
    return "Prestige : " + "{}".format(self.prestige)

  def displayNobleTile(self):
    print "============== Noble ==============="
    print " Prestige = ", self.prestige
    print " Requirements = ", str( self.requirements )
    print "------------------------------------"


class AgentState:
  """
  AgentStates hold the state of an agent:
    (tokens, jokers, devCards, nobleTiles, devCardReserves).
  """
  def __init__( self ):
    # ['Green', 'Blue', 'Red', 'White', 'Black']
    self.tokens = (0, 0, 0, 0, 0)
    # ['Yellow']
    self.jokers = 0
    # List of DevelopmentCard(s)
    self.devCards = []
    # List of NobleTile(s)
    self.nobleTiles = []
    # List of DevelopmentCard(s)
    self.devCardReserves = []

  def getTokens(self):
    return self.tokens

  def getJokers(self):
    return self.jokers

  def getDevCards(self):
    return self.devCards

  def getNobleTiles(self):
    return self.nobleTiles

  def getDevCardReserves(self):
    return self.devCardReserves

  def getPrestige(self):
    prestige = 0
    for devCard in self.devCards:
      prestige += devCard.getPrestige()
    for nobleTile in self.nobleTiles:
      prestige += nobleTile.getPrestige()
    return prestige

  def __str__( self ):
    return "Prestige : " + str( self.getPrestige() )

  def __hash__(self):
    devCardsTpl = tuple(self.devCards)
    nobleTilesTpl = tuple(self.nobleTiles)
    devCardReservesTpl = tuple(self.devCardReserves)
    retValToHash = ( self.tokens, self.jokers, devCardsTpl, \
                     nobleTilesTpl, devCardReservesTpl )
    return hash(retValToHash)

  def displayAgentState( self ):
    for devCard in self.devCards:
      devCard.displayDevCard()
    for nobleTile in self.nobleTiles:
      nobleTile.displayNobleTile()
    print "===== Tokens ====="
    print " Tokens = ", self.tokens
    print "===== Jokers ====="
    print " Jokers = ", self.jokers
    print "===== Prestige ====="
    print " Total Prestige = ", self.getPrestige()
    print "--------------------"


class TableState:
  """
  TableState holds the state of the table:
    (tokens, jokers, devCards (face-up), nobleTiles, decks).
    - tokens available
    - jokers available
    - devCards facing-up arranged in 3 rows of 4 cards each.
      one row per tier/deck.
    - nobleTiles available (have not visit any merchant players yet.)
    - 3 decks, 1 per tier.
  """

  # Game Setup
  # Number of players may enforce some rules on multiplicity, numberOfJokers, and nobleTiles
  # This is enforced at a higher level -> Mindset: this is just a dumb table.
  # However, as dumb as it may be, this table holds every piece of the game already.
  # As the game progresses, the pieces are scattered amongst the agents/players/merchants.
  # But there are no extra pieces thrown into the game but the ones used to initialized an
  # instance of TableState.
  def __init__( self, devCards=[], decks=[], nobleTiles=[], tokenMultiplicity=7, numberOfJokers=5):
    self.tokenMultiplicity = tokenMultiplicity
    self.jokerMultiplicity = numberOfJokers
    self.nobleMultiplicity = len(nobleTiles)
    # ['Green', 'Blue', 'Red', 'White', 'Black']
    self.tokens = (self.tokenMultiplicity,) * len(GemTypesColors)
    # ['Yellow']
    self.jokers = numberOfJokers
    # List of NobleTile(s) available for potential visits.
    self.nobleTiles = nobleTiles
    # List of 3 lists of DevelopmentCard(s), one list per row/tier/deck.
    self.devCards = devCards
    # List of 3 stacks/decks. Each deck is a stack of devCards with a predetermined but hidden order.
    # No agent can peak the order... unless we are testing an Oracle agent.
    self.decks = decks

  # String representation of TableState
  def __str__(self):
    return " Tokens: {} \n".format( self.tokens ) + \
           " Jokers: {} \n".format( self.jokers ) + \
           " NobleTiles: {} \n".format( len(self.nobleTiles) ) + \
           " Deck cards left: #1:{} #2:{} #3:{} \n".format(len(self.decks[0]),len(self.decks[1]),len(self.decks[2]))

  def __hash__(self):
    # Change to tuple
    nobleTilesTpl = tuple(self.nobleTiles)
    # Change to tuple of 3 tuples of devCards
    devCardsTempList = [tuple(devCardRowList) for devCardRowList in self.devCards]
    devCardsTpl = tuple(devCardsTempList)
    # Change to tuple of 3 tuples of devCards
    decksTempList = [tuple(deckTierStack) for deckTierStack in self.decks]
    decksTpl = tuple(decksTempList)

    retValToHash = (self.tokens, self.jokers, nobleTilesTpl, devCardsTpl, decksTpl)
    return hash(retValToHash)

  # Don't display.. there's too much info. Maybe drawing is better at this point?
  def displayTableState():
    pass


class GameState:
  def __init__( self, numAgents=0, tableState=None, prestigeToWin=15, startingAgentIndex=0, prevState=None):
    if prevState is None:
      # Main player is agent at index zero
      self.agentStates = [AgentState() for _ in range(numAgents)]
      self.prestigeToWin = prestigeToWin
      self.fullRound = True
      # Initial game state / splendor setup
      self.tableState = tableState
      self.whoseTurn = startingAgentIndex
      self.startingAgentIndex = startingAgentIndex
      self.numberRounds = 1
    else:
      self.agentStates = [copy.deepcopy(agentState) for agentState in prevState.agentStates]
      self.prestigeToWin = prevState.prestigeToWin
      self.fullRound = prevState.fullRound
      self.tableState = copy.deepcopy(prevState.tableState)
      self.whoseTurn = prevState.whoseTurn
      self.startingAgentIndex = prevState.startingAgentIndex
      self.numberRounds = prevState.numberRounds

  def __str__( self ):
    return " Progress: {}% \n".format( self.getProgress() ) + \
           " Rounds: #{} \n".format( self.numberRounds ) + \
           " Turn: #{} \n".format( self.whoseTurn ) + \
           str(self.tableState)

  def utility( self , agentIndex):
    # Main player is agent at index zero.
    if not self.isEndState():
      raise Exception("Game hasn't ended! You can't get a utility.")
    if self.isWin(agentIndex):
      return 20.
    else:
      return -20.

  def getProgress( self ):
    maxAgentPrestige = 0
    for agentState in self.agentStates:
      prestige = agentState.getPrestige()
      if prestige > maxAgentPrestige:
        maxAgentPrestige = prestige
    return (float(maxAgentPrestige)/self.prestigeToWin) * 100.0

  def nextTurn( self ):
    self.whoseTurn = (self.whoseTurn + 1) % len(self.agentStates)
    self.fullRound = (self.whoseTurn == self.startingAgentIndex)
    if self.fullRound:
      self.numberRounds += 1

  def isEndState( self ):
    if not self.fullRound: return False
    for agentState in self.agentStates:
      if agentState.getPrestige() >= self.prestigeToWin:
        return True
    return False

  def isWin( self , agentIndex=0):
    # Main player is agent at index zero. Tie == Lost
    if not self.isEndState(): return False
    for i,agentState in enumerate(self.agentStates):
      if i != agentIndex:
        if agentState.getPrestige() >= self.agentStates[agentIndex].getPrestige():
          return False
    return True

  def isLose( self , agentIndex=0):
    # Main player is agent at index zero. Tie == Lost
    if not self.isEndState(): return False
    return not self.isWin(agentIndex)

  def getWinnerAgentIndex( self ):
    if not self.isEndState(): return None
    winner = -1
    maxScore = -1
    for i,agent in enumerate(self.agentStates):
      prestige = agent.getPrestige()
      if prestige >= self.prestigeToWin and prestige > maxScore:
        winner = i
        maxScore = prestige
    return winner

  def checkAgentNumberOfTokens( self ):
    agent = self.agentStates[self.whoseTurn]
    total = sum(agent.tokens) + agent.jokers
    # Max number of tokens allowed per agent is 10.
    if total <= 10: return 0
    return total - 10

  def getLegalActions(self, agentIndex):
    """
    Return a list of legal/valid actions for this gameState and agentIndex.
    Legal Actions are lists of key strokes to leverage game rules validation
    built for the KeyboardAgent.

    These are all the possible actions for a given agent. This method builds
    a list of legal actions given the current gamestate (self) effectively
    filtering out the below list.

      1 BuyDevCard
        [ '1', tier, column ]
        tier   E{'1','2','3'}
        column E{'1','2','3','4'}

      2 Take2EqualGems
        [ '2', gemIndex ]
        gemIndex E{'1','2','3','4','5'}

      3 Take3DiffGems
        [ '3', gemIndex1, gemIndex2, gemIndex3 ]
        gemIndex1, gemIndex2, gemIndex3 E{'1','2','3','4','5'}

      4 ReserveDevCard
        [ '4', tier, column ]
        tier   E{'1','2','3'}
        column E{'0','1','2','3','4'}
        Column 0: deck top.

      5 BuyReservedDevCard
        [ '5', reserveIndex ]
        reserveIndex E{'1','2','3'}

    """
    if self.isEndState(): return []

    # print "getLegalActions call from agentIndex=",agentIndex

    actionsList = []
    if agentIndex != self.whoseTurn: return []

    agent = self.agentStates[agentIndex]
    totalTokens = sum(agent.tokens) + agent.jokers
    agentMaxTokens = (totalTokens == 10)
    gameRules = GameRules()
    zeroOrd = ord('0')

    # -------------------------------------------------------
    # Add any possible |Actions.BuyDevCard| action
    # -------------------------------------------------------
    for tier in range(1,4):
      for column in range(1,5):
        # Card exists?
        devCard = self.tableState.devCards[tier-1][column-1]
        if not gameRules.canBuyDevCard(self, agentIndex, devCard): continue
        actionsList.append( [zeroOrd+1, zeroOrd+tier, zeroOrd+column] )

    # -------------------------------------------------------
    # Add any possible |Actions.Take2EqualGems| action
    # -------------------------------------------------------
    if not agentMaxTokens:
      for gemIndex in range(1,6):
        if self.tableState.tokens[gemIndex-1] < 4: continue
        actionsList.append( [zeroOrd+2, zeroOrd+gemIndex] )

    # -------------------------------------------------------
    # Add any possible |Actions.Take3DiffGems| action
    # -------------------------------------------------------
    if not agentMaxTokens:
      for gemIndex1 in range(1,4):
        for gemIndex2 in range(gemIndex1+1,5):
          if gemIndex1 == gemIndex2: continue
          for gemIndex3 in range(gemIndex2+1,6):
            if gemIndex1 == gemIndex3: continue
            if gemIndex2 == gemIndex3: continue

            illegalAction = False
            numberGemTypesAvailable = sum([1 if gemTypeToken != 0 else 0 \
                                          for gemTypeToken in self.tableState.tokens])
            if numberGemTypesAvailable == 0:
              illegalAction = True

            gemIndexes = [gemIndex1-1, gemIndex2-1, gemIndex3-1]
            # Less than 3 gemTypes available: allow for less gems.
            if numberGemTypesAvailable < 3:
              tmpIndexes = []
              for index in gemIndexes:
                if self.tableState.tokens[index] != 0:
                  tmpIndexes.append(index)
              gemIndexes = tmpIndexes
              if len(gemIndexes) == 0:
                illegalAction = True

            # At least 3 gemTypes available
            else:
              for index in gemIndexes:
                if self.tableState.tokens[index] == 0:
                  illegalAction = True
                  break

            if illegalAction: continue
            actionsList.append( [zeroOrd+3, zeroOrd+gemIndex1, \
                                            zeroOrd+gemIndex2, \
                                            zeroOrd+gemIndex3] )

    # -------------------------------------------------------
    # Add any possible |Actions.ReserveDevCard| action
    # -------------------------------------------------------
    if len(agent.devCardReserves) < 3:
      for tier in range(1,4):
        for column in range(0,5):
          illegalAction = False
          # Reserving from deckTop.
          if column == 0:
            # Check if deck tier not empty.
            deck = self.tableState.decks[tier-1]
            if len(deck) == 0:
              illegalAction = True
          # Reserving from table face-up devCards.
          else:
            # Check if card exists at location.
            devCard = self.tableState.devCards[tier-1][column-1]
            if devCard is None:
              illegalAction = True

          if illegalAction: continue
          actionsList.append( [zeroOrd+4, zeroOrd+tier, zeroOrd+column] )

    # -------------------------------------------------------
    # Add any possible |Actions.BuyReservedDevCard| action
    # -------------------------------------------------------
    for resIndex in range(1,4):
      # Check if reserve index exists!
      if resIndex > len(agent.devCardReserves): continue
      # Check if agent buyer has enough gem cash.
      devCard = agent.devCardReserves[resIndex-1]
      if not gameRules.canBuyDevCard(self, agentIndex, devCard): continue
      actionsList.append( [zeroOrd+5, zeroOrd+resIndex] )

    # If no moves are possible, agent can always do a NoOp and pass.
    if len(actionsList) == 0:
      actionsList.append( [zeroOrd+6] )

    return actionsList


  def generateSuccessorWithChance(self, agentIndex, action):
    """
    Returns the successor state after the specified agent takes the action.
    """
    # Check that the successor exist
    if self.isEndState():
      raise Exception("Cannot generate a successor of a terminal state.")

    # Copy current gameState
    nextGS = GameState(prevState = self)
    if action == None:
      return nextGS

    # Mutate the gameState into the next gameState before returning it.
    # Assume action is legal since it should come from getLegalActions().
    zeroOrd = ord('0')
    gameRules = GameRules()

    """
    --------------------------------------------------------------------
    The agentIndex usually corresponds to one of the 2-4 players/agents.
    However, for modeling purposes we introduce another actor: Chance.
    Who's in charge of drawing a development card form the decks.
    Chance agentIndex = -1
    --------------------------------------------------------------------
    """
    if agentIndex == -1:
      """
                      action = [tier, column, index]
      Here, action represents a specific development card (index) already in
      the deck tier specified. This developement card is one of the many
      possible cards that could be drawn before the next player's turn.

      tier E{1,2,3} and 0 <= index < len(deck[tier-1]) and column E{0,1,2,3,4}.

      Where index = 0, represents the top most development card. Also, column
      indicates the empty slot to fill in with the drawn development card. If
      column = 0 then the actions indicates a direct reserve from the deck top.
      Thus, the card is granted to the corresponding agentState.
      """
      tier = action[0] - zeroOrd
      column = action[1] - zeroOrd
      index = action[2]
      deck = nextGS.tableState.decks[tier-1]
      possibleDevCard = deck.pop(index)

      if column != 0:
        # Replace the empty slot in tier column. Agent may have bought or reserved a card.
        nextGS.tableState.devCards[tier-1][column-1] = possibleDevCard
      else:
        # Grant reserve development card from deckTop to agentState.
        nextGS.agentStates[nextGS.whoseTurn].devCardReserves.append(possibleDevCard)

      # When a development card is to be drawn the turn is incremented once drawn and
      # not when a previous Agent action launches a Chance action.
      nextGS.nextTurn()


    elif agentIndex == nextGS.whoseTurn:
      # This should be the same for all agents.
      # Look at the action
      actionType = action[0] - zeroOrd
      # --------------------------------------------------------------------
      # Generate next gameState based on action: BuyDevCard
      # --------------------------------------------------------------------
      if actionType == Actions.BuyDevCard.value:
        # action = [Action, tier, column]
        tier = action[1] - zeroOrd
        column = action[2] - zeroOrd

        # Take devCard from table.
        devCard = nextGS.tableState.devCards[tier-1][column-1]
        nextGS.tableState.devCards[tier-1][column-1] = None

        # Charge Agent
        isItForFree = gameRules.isItForFree(nextGS, devCard.cost)
        remainingTokensToCharge = None
        if isItForFree:
          # 5 gem Types + jokers
          remainingTokensToCharge = (0,0,0,0,0,0)
        else:
          # If cant afford, returns None
          remainingTokensToCharge = gameRules.getTokensToCharge(nextGS, devCard.cost)
        if not (isItForFree or remainingTokensToCharge != None):
          raise Exception("BuyDevCard action turned out to be illegal!")
        # At this point, we can complete the purchase. Charge agent.
        gameRules.chargeAgent(nextGS, remainingTokensToCharge)

        # Grant devCard to agent. It's assume the agent can afford this devCard.
        nextGS.agentStates[nextGS.whoseTurn].devCards.append(devCard)
        # Before returning, check if agent/player reached prestige requirements for a Noble visit.
        gameRules.checkNobleRequirements(nextGS)

        # We still haven't replaced the devCard.
        # The caller must launch a Chance Action immediately after this to ready
        # the gameState before the next agent's/player's turn.

      # --------------------------------------------------------------------
      # Generate next gameState based on action: Take2EqualGems
      # --------------------------------------------------------------------
      elif actionType == Actions.Take2EqualGems.value:
        # action = [Action, gemType]
        gemIndex = action[1] - zeroOrd - 1
        gemType = GemType[gemIndex]
        if not gameRules.grantTwoTokens(nextGS, gemType):
          raise Exception("Take2EqualGems action turned out to be illegal!")

        # Check if agent/player must discard tokens if max exceeded.
        numTokensToDiscard = nextGS.checkAgentNumberOfTokens()
        if numTokensToDiscard:
          gemList = []
          i = 1
          agentState = nextGS.agentStates[nextGS.whoseTurn]
          while len(gemList) < numTokensToDiscard:
            # Start-off with a different gemType than the one we just took 2 from.
            gemIndex = (gemIndex + 1) % len(GemType)
            i += 1
            if agentState.tokens[gemIndex] == 0: continue
            # Can agent afford this? If not, continue ignoring. Avoid going to negative tokens when discarding.
            tmpGemList = list(gemList)
            tmpGemList.append(GemType[gemIndex])
            count = Counter(tmpGemList)
            cannotAfford = False
            for gemType in tmpGemList:
              if agentState.tokens[GemType.index(gemType)] < count[gemType]:
                cannotAfford = True
                break
            if cannotAfford: continue
            gemList.append(GemType[gemIndex])

          options = dict()
          for i, gem in enumerate(gemList):
            options['gemType'+str(i+1)] = gem
          gameRules.discardTokens(nextGS, options)
        nextGS.nextTurn()


      # --------------------------------------------------------------------
      # Generate next gameState based on action: Take3DiffGems
      # --------------------------------------------------------------------
      elif actionType == Actions.Take3DiffGems.value:
        # action = [Action, gemType1, gemType2, gemType3]
        gemType1 = GemType[action[1] - zeroOrd - 1]
        gemType2 = GemType[action[2] - zeroOrd - 1]
        gemType3 = GemType[action[3] - zeroOrd - 1]
        gemIndex = action[3] - zeroOrd - 1
        if not gameRules.grantThreeTokens(nextGS, gemType1, gemType2, gemType3):
          raise Exception("Take3DiffGems action turned out to be illegal!")

        # Check if agent/player must discard tokens if max exceeded.
        numTokensToDiscard = nextGS.checkAgentNumberOfTokens()
        if numTokensToDiscard:
          gemList = []
          i = 1
          agentState = nextGS.agentStates[nextGS.whoseTurn]
          while len(gemList) < numTokensToDiscard:
            # Start-off with a different gemType than the one we just took 2 from.
            gemIndex = (gemIndex + 1) % len(GemType)
            i += 1
            if agentState.tokens[gemIndex] == 0: continue
            # Can agent afford this? If not, continue ignoring. Avoid going to negative tokens when discarding.
            tmpGemList = list(gemList)
            tmpGemList.append(GemType[gemIndex])
            count = Counter(tmpGemList)
            cannotAfford = False
            for gemType in tmpGemList:
              if agentState.tokens[GemType.index(gemType)] < count[gemType]:
                cannotAfford = True
                break
            if cannotAfford: continue
            gemList.append(GemType[gemIndex])

          options = dict()
          for i, gem in enumerate(gemList):
            options['gemType'+str(i+1)] = gem
          gameRules.discardTokens(nextGS, options)
        nextGS.nextTurn()


      # --------------------------------------------------------------------
      # Generate next gameState based on action: ReserveDevCard
      # --------------------------------------------------------------------
      elif actionType == Actions.ReserveDevCard.value:
        # action = [Action, tier, column]
        tier = action[1] - zeroOrd
        column = action[2] - zeroOrd

        # Reserving from deckTop.
        if column == 0:
          # It is assume deck is not empty.
          # Nothing to do here. The Chance Action to be launched right after this should
          # update agentState with new reserve drawn from deck.
          # We still have to grant tokens if possible.
          pass

        # Reserving from table face-up devCards.
        else:
          # It is assume devCard exists at the location specified by (tier,column)
          # Take devCard from table.
          devCard = nextGS.tableState.devCards[tier-1][column-1]
          nextGS.tableState.devCards[tier-1][column-1] = None
          # It is assume the agent can reserve this card. Update agentState with new reserve.
          nextGS.agentStates[nextGS.whoseTurn].devCardReserves.append(devCard)

          # We still haven't replaced the devCard.
          # The caller must launch a Chance Action immediately after this to ready
          # the gameState before the next agent's/player's turn.

        # To grant a reservation Gold Token a.k.a. Joker we must:
        # Check if agent has less than 10 tokens+jokers and there are table jokers available.
        # This joker grant is not needed for a reservation to occur - is more like a bonus.
        agent = nextGS.agentStates[nextGS.whoseTurn]
        table = nextGS.tableState
        totalTokens = sum(agent.tokens) + agent.jokers
        if (table.jokers > 0) and (totalTokens < 10):
          # Grant Gold Token / Joker!
          table.jokers -= 1
          agent.jokers += 1

      # --------------------------------------------------------------------
      # Generate next gameState based on action: BuyReservedDevCard
      # --------------------------------------------------------------------
      elif actionType == Actions.BuyReservedDevCard.value:
        # action = [Action, index]
        index = action[1] - zeroOrd

        # Take card from reserves. It is assume the agent can afford this devCard.
        # This also removes devCard from list of reserves.
        devCard = nextGS.agentStates[nextGS.whoseTurn].devCardReserves.pop(index-1)
        # In this action case, there is no devCard to replace.

        # Charge Agent
        isItForFree = gameRules.isItForFree(nextGS, devCard.cost)
        remainingTokensToCharge = None
        if isItForFree:
          # 5 gem Types + jokers
          remainingTokensToCharge = (0,0,0,0,0,0)
        else:
          # If cant afford, returns None
          remainingTokensToCharge = gameRules.getTokensToCharge(nextGS, devCard.cost)
        if not (isItForFree or remainingTokensToCharge != None):
          raise Exception("BuyDevCard action turned out to be illegal!")
        # At this point, we can complete the purchase. Charge agent.
        gameRules.chargeAgent(nextGS, remainingTokensToCharge)

        # Grant devCard to agent. It's assume the agent can afford this devCard.
        nextGS.agentStates[nextGS.whoseTurn].devCards.append(devCard)
        # Before returning, check if agent/player reached prestige requirements for a Noble visit.
        gameRules.checkNobleRequirements(nextGS)

        # In this action case, there is no Chance Action to launch.
        nextGS.nextTurn()


      # --------------------------------------------------------------------
      # Generate next gameState based on action: NoOp
      # --------------------------------------------------------------------
      elif actionType == Actions.NoOp.value:
        # action = [Action]
        # Do nothing.
        nextGS.nextTurn()

      else:
        raise Exception("action type not recognized!")


    else:
      raise Exception("agentIndex does not match the agent in turn!")

    return nextGS




  def generateSuccessorNoChance(self, agentIndex, action):
    """
    Returns the successor state after the specified agent takes the action.
    """
    # Check that the successor exist
    if self.isEndState():
      raise Exception("Cannot generate a successor of a terminal state.")

    # Copy current gameState
    nextGS = GameState(prevState = self)
    if action == None:
      return nextGS

    # Mutate the gameState into the next gameState before returning it.
    # Assume action is legal since it should come from getLegalActions().
    zeroOrd = ord('0')
    gameRules = GameRules()


    if agentIndex == nextGS.whoseTurn:
      # This should be the same for all agents.
      # Look at the action
      actionType = action[0] - zeroOrd
      # --------------------------------------------------------------------
      # Generate next gameState based on action: BuyDevCard
      # --------------------------------------------------------------------
      if actionType == Actions.BuyDevCard.value:
        # action = [Action, tier, column]
        tier = action[1] - zeroOrd
        column = action[2] - zeroOrd

        # Take devCard from table.
        devCard = nextGS.tableState.devCards[tier-1][column-1]
        nextGS.tableState.devCards[tier-1][column-1] = None

        # Charge Agent
        isItForFree = gameRules.isItForFree(nextGS, devCard.cost)
        remainingTokensToCharge = None
        if isItForFree:
          # 5 gem Types + jokers
          remainingTokensToCharge = (0,0,0,0,0,0)
        else:
          # If cant afford, returns None
          remainingTokensToCharge = gameRules.getTokensToCharge(nextGS, devCard.cost)
        if not (isItForFree or remainingTokensToCharge != None):
          raise Exception("BuyDevCard action turned out to be illegal!")
        # At this point, we can complete the purchase. Charge agent.
        gameRules.chargeAgent(nextGS, remainingTokensToCharge)

        # Grant devCard to agent. It's assume the agent can afford this devCard.
        nextGS.agentStates[nextGS.whoseTurn].devCards.append(devCard)
        # Before returning, check if agent/player reached prestige requirements for a Noble visit.
        gameRules.checkNobleRequirements(nextGS)

        # Replaced the devCard.
        deck = nextGS.tableState.decks[tier-1]
        if len(deck) != 0:
          nextDevCard = deck.pop(0)
          nextGS.tableState.devCards[tier-1][column-1] = nextDevCard
        nextGS.nextTurn()


      # --------------------------------------------------------------------
      # Generate next gameState based on action: Take2EqualGems
      # --------------------------------------------------------------------
      elif actionType == Actions.Take2EqualGems.value:
        # action = [Action, gemType]
        gemIndex = action[1] - zeroOrd - 1
        gemType = GemType[gemIndex]
        if not gameRules.grantTwoTokens(nextGS, gemType):
          raise Exception("Take2EqualGems action turned out to be illegal!")

        # Check if agent/player must discard tokens if max exceeded.
        numTokensToDiscard = nextGS.checkAgentNumberOfTokens()
        if numTokensToDiscard:
          gemList = []
          i = 1
          agentState = nextGS.agentStates[nextGS.whoseTurn]
          while len(gemList) < numTokensToDiscard:
            # Start-off with a different gemType than the one we just took 2 from.
            gemIndex = (gemIndex + 1) % len(GemType)
            i += 1
            if agentState.tokens[gemIndex] == 0: continue
            # Can agent afford this? If not, continue ignoring. Avoid going to negative tokens when discarding.
            tmpGemList = list(gemList)
            tmpGemList.append(GemType[gemIndex])
            count = Counter(tmpGemList)
            cannotAfford = False
            for gemType in tmpGemList:
              if agentState.tokens[GemType.index(gemType)] < count[gemType]:
                cannotAfford = True
                break
            if cannotAfford: continue
            gemList.append(GemType[gemIndex])

          options = dict()
          for i, gem in enumerate(gemList):
            options['gemType'+str(i+1)] = gem
          gameRules.discardTokens(nextGS, options)
        nextGS.nextTurn()


      # --------------------------------------------------------------------
      # Generate next gameState based on action: Take3DiffGems
      # --------------------------------------------------------------------
      elif actionType == Actions.Take3DiffGems.value:
        # action = [Action, gemType1, gemType2, gemType3]
        gemType1 = GemType[action[1] - zeroOrd - 1]
        gemType2 = GemType[action[2] - zeroOrd - 1]
        gemType3 = GemType[action[3] - zeroOrd - 1]
        gemIndex = action[3] - zeroOrd - 1
        if not gameRules.grantThreeTokens(nextGS, gemType1, gemType2, gemType3):
          raise Exception("Take3DiffGems action turned out to be illegal!")

        # Check if agent/player must discard tokens if max exceeded.
        numTokensToDiscard = nextGS.checkAgentNumberOfTokens()
        if numTokensToDiscard:
          gemList = []
          i = 1
          agentState = nextGS.agentStates[nextGS.whoseTurn]
          while len(gemList) < numTokensToDiscard:
            # Start-off with a different gemType than the one we just took 2 from.
            gemIndex = (gemIndex + 1) % len(GemType)
            i += 1
            if agentState.tokens[gemIndex] == 0: continue
            # Can agent afford this? If not, continue ignoring. Avoid going to negative tokens when discarding.
            tmpGemList = list(gemList)
            tmpGemList.append(GemType[gemIndex])
            count = Counter(tmpGemList)
            cannotAfford = False
            for gemType in tmpGemList:
              if agentState.tokens[GemType.index(gemType)] < count[gemType]:
                cannotAfford = True
                break
            if cannotAfford: continue
            gemList.append(GemType[gemIndex])

          options = dict()
          for i, gem in enumerate(gemList):
            options['gemType'+str(i+1)] = gem
          gameRules.discardTokens(nextGS, options)
        nextGS.nextTurn()


      # --------------------------------------------------------------------
      # Generate next gameState based on action: ReserveDevCard
      # --------------------------------------------------------------------
      elif actionType == Actions.ReserveDevCard.value:
        # action = [Action, tier, column]
        tier = action[1] - zeroOrd
        column = action[2] - zeroOrd

        # Reserving from deckTop.
        if column == 0:
          # It is assume deck is not empty.
          # We still have to grant tokens if possible.
          # Grant reserve development card from deckTop to agentState.
          deck = nextGS.tableState.decks[tier-1]
          nextDevCard = deck.pop(0)
          nextGS.agentStates[nextGS.whoseTurn].devCardReserves.append(nextDevCard)
          nextGS.nextTurn()

        # Reserving from table face-up devCards.
        else:
          # It is assume devCard exists at the location specified by (tier,column)
          # Take devCard from table.
          devCard = nextGS.tableState.devCards[tier-1][column-1]
          nextGS.tableState.devCards[tier-1][column-1] = None
          # It is assume the agent can reserve this card. Update agentState with new reserve.
          nextGS.agentStates[nextGS.whoseTurn].devCardReserves.append(devCard)

          # Replaced the devCard.
          deck = nextGS.tableState.decks[tier-1]
          if len(deck) != 0:
            nextDevCard = deck.pop(0)
            nextGS.tableState.devCards[tier-1][column-1] = nextDevCard
          nextGS.nextTurn()

        # To grant a reservation Gold Token a.k.a. Joker we must:
        # Check if agent has less than 10 tokens+jokers and there are table jokers available.
        # This joker grant is not needed for a reservation to occur - is more like a bonus.
        agent = nextGS.agentStates[nextGS.whoseTurn]
        table = nextGS.tableState
        totalTokens = sum(agent.tokens) + agent.jokers
        if (table.jokers > 0) and (totalTokens < 10):
          # Grant Gold Token / Joker!
          table.jokers -= 1
          agent.jokers += 1

      # --------------------------------------------------------------------
      # Generate next gameState based on action: BuyReservedDevCard
      # --------------------------------------------------------------------
      elif actionType == Actions.BuyReservedDevCard.value:
        # action = [Action, index]
        index = action[1] - zeroOrd

        # Take card from reserves. It is assume the agent can afford this devCard.
        # This also removes devCard from list of reserves.
        devCard = nextGS.agentStates[nextGS.whoseTurn].devCardReserves.pop(index-1)
        # In this action case, there is no devCard to replace.

        # Charge Agent
        isItForFree = gameRules.isItForFree(nextGS, devCard.cost)
        remainingTokensToCharge = None
        if isItForFree:
          # 5 gem Types + jokers
          remainingTokensToCharge = (0,0,0,0,0,0)
        else:
          # If cant afford, returns None
          remainingTokensToCharge = gameRules.getTokensToCharge(nextGS, devCard.cost)
        if not (isItForFree or remainingTokensToCharge != None):
          raise Exception("BuyDevCard action turned out to be illegal!")
        # At this point, we can complete the purchase. Charge agent.
        gameRules.chargeAgent(nextGS, remainingTokensToCharge)

        # Grant devCard to agent. It's assume the agent can afford this devCard.
        nextGS.agentStates[nextGS.whoseTurn].devCards.append(devCard)
        # Before returning, check if agent/player reached prestige requirements for a Noble visit.
        gameRules.checkNobleRequirements(nextGS)

        # In this action case, there is no Chance Action to launch.
        nextGS.nextTurn()


      # --------------------------------------------------------------------
      # Generate next gameState based on action: NoOp
      # --------------------------------------------------------------------
      elif actionType == Actions.NoOp.value:
        # action = [Action]
        # Do nothing.
        nextGS.nextTurn()

      else:
        raise Exception("action type not recognized!")


    else:
      raise Exception("agentIndex does not match the agent in turn!")

    return nextGS




class Game:
  """
  The Game manages the control flow, soliciting actions from agents.
  """
  def __init__(self, displayOn, agents, tableSetup, startingIndex=0, prestigeToWin=15):
    self.agents = agents
    self.startingIndex = startingIndex
    self.gameOver = False
    self.gameState = GameState(len(self.agents), tableSetup, prestigeToWin, startingIndex)
    self.roundsNumber = 0
    self.gameRules = GameRules()
    self.displayOn = displayOn
    self.episode = []

  def run( self ):
    """
    Main control loop for game play - delegate to curses display
    """
    if self.displayOn:
      curses.wrapper(self.displayGameStateCurses)
    else:
      self.displayGameStateCurses()

    # Print winner and wrap-up.
    if self.gameOver:
      # Log end state! and final reward for agentIndex ZERO.
      # Replace last zero'ed reward with actual game reward.
      self.episode = self.episode[:-1]
      self.episode.append( self.gameState.utility(0) )
      self.episode.append( GameState(prevState = self.gameState) )

      winner = self.gameState.getWinnerAgentIndex()
      prestige = self.gameState.agentStates[winner].getPrestige()
      print "Winner: Agent#{} with Prestige={} in {} Rounds".format(winner, \
            prestige, self.roundsNumber)
      return [True, prestige, self.roundsNumber, winner]
    return [False, None, None, None]


  def displayGameStateCurses( self, stdscr=None ):
    if self.displayOn:
      # Clear and refresh the screen for a blank canvas. Start colors.
      stdscr.clear()
      stdscr.refresh()
      self.startColor()

    # Init game loop
    numAgents = len( self.agents )
    self.roundsNumber = 1
    keyPressed = 0
    lastMove = Actions.BuyDevCard
    lastNMoves = [lastMove for _ in range(numAgents)]

    # Loop where keyPressed is the last character pressed
    while (keyPressed != 27) and not self.gameOver:

      height = 0
      width = 0
      if self.displayOn:
        # Next Screen Initialization
        stdscr.erase()
        height, width = stdscr.getmaxyx()

        # Render Game
        self.drawTitleBorder(stdscr, height, width)
        self.drawStatusBar(stdscr, height, width, keyPressed, "Last: "+ActionStr[lastMove.value-1])
        self.drawBank(stdscr, height, width)
        self.drawTableNobleTiles(stdscr, height, width)
        self.drawTableDeckAndDevCards(stdscr, height, width)
        self.drawAgentStates(stdscr, height, width, self.gameState.whoseTurn)
        # Refresh the screen
        stdscr.refresh()

      # Fetch the next agent
      agent = self.agents[self.gameState.whoseTurn]

      notValidAction = True
      while notValidAction and (keyPressed != 27):
        # Debug Stop
        # debug = stdscr.getch()
        # if debug == 27: sys.exit(0)

        # Observe
        if not agent.isKeyboardAgent:
          # Store in agent.simKeyStrokes
          agent.getAction(self.gameState)
          # print "agent#{} action : agent.simKeyStrokes={}".format(self.gameState.whoseTurn,\
          #                                                         agent.simKeyStrokes)

        if self.displayOn:
          # Display possible Actions for agent.
          self.drawPossibleActions(stdscr, height, width)

        # Play Action - wait for next input.
        if agent.isKeyboardAgent:
          keyPressed = stdscr.getch()
        else:
          keyPressed = agent.simKeyStrokes.pop(0)

        if self.displayOn:
          # Delete Invalid Action Msg by overwriting with the title fo the Game!
          self.drawTitleBorder(stdscr, height, width)
          self.drawStatusBar(stdscr, height, width, keyPressed)

        # Somehow translate keys into actions.
        action = None
        if keyPressed == ord('1'):
          options = self.askForDevCardLoc(stdscr, height, width)
          if options is not None:
            action = Actions.BuyDevCard
            lastMove = action
        elif keyPressed == ord('2'):
          options = self.askFor2EqualGems(stdscr, height, width)
          if options is not None:
            action = Actions.Take2EqualGems
            lastMove = action
        elif keyPressed == ord('3'):
          options = self.askFor3DiffGems(stdscr, height, width)
          if options is not None:
            action = Actions.Take3DiffGems
            lastMove = action
        elif keyPressed == ord('4'):
          options = self.askForDevCardLoc(stdscr, height, width, True)
          if options is not None:
            action = Actions.ReserveDevCard
            lastMove = action
        elif keyPressed == ord('5'):
          options = self.askForReserveLoc(stdscr, height, width)
          if options is not None:
            action = Actions.BuyReservedDevCard
            lastMove = action
        elif keyPressed == ord('6'):
          options = []
          action = Actions.NoOp
          lastMove = action
          # End-game if all agents have passed consecutively.
          endGame = True
          for move in lastNMoves:
            if move != Actions.NoOp:
              endGame = False
              break
          if endGame: keyPressed = 27

        lastNMoves[self.gameState.whoseTurn] = action

        if not agent.isKeyboardAgent:
          # Copy previous (state, action) before modifying.
          prevGameState = GameState(prevState = self.gameState)
          prevActionKeyStrokes = list(agent.lastSimKeyStrokes)

        # Validate Agent's action.
        # GameRules update GameState with Action from Agent.
        notValidAction = (action is None) or \
                         (not self.gameRules.applyAction(action, options, self.gameState))

        if notValidAction and self.displayOn:
          self.drawInvalidActionMsg(stdscr, height, width)
        elif not notValidAction and not agent.isKeyboardAgent:
          # Upon valid action correctly applied, log previous gameState, action and reward.
          # Do it only if this isn't a human agent due to the different action format.
          self.episode.append(prevGameState)
          self.episode.append(prevActionKeyStrokes)
          self.episode.append(0)

      if (keyPressed == 27): break

      # Check if agent/player must discard tokens if max exceeded.
      numTokensToDiscard = self.gameState.checkAgentNumberOfTokens()
      if numTokensToDiscard:
        options = self.askForGemsToDiscard(stdscr, height, width, numTokensToDiscard, self.gameState)
        self.gameRules.applyAction(Actions.DiscardTokens, options, self.gameState)

      # Next agent
      self.gameState.nextTurn()
      if self.gameState.fullRound: self.roundsNumber += 1

      # Game ended?
      self.gameOver = self.gameState.isEndState()

    return

  def askForGemsToDiscard(self, stdscr, height, width, numTokensToDiscard, gameState):
    agentState = gameState.agentStates[gameState.whoseTurn]
    gemList = []
    while len(gemList) < numTokensToDiscard:
      gemIndex = self.getGemIndexAndValidate(stdscr, height, width, True)
      if gemIndex == None: continue
      if agentState.tokens[gemIndex] == 0: continue
      # Can agent afford this? If not, continue ignoring. Avoid going to negative tokens when discarding.
      tmpGemList = list(gemList)
      tmpGemList.append(GemType[gemIndex])
      count = Counter(tmpGemList)
      cannotAfford = False
      for gemType in tmpGemList:
        if agentState.tokens[GemType.index(gemType)] < count[gemType]:
          cannotAfford = True
          break
      if cannotAfford: continue

      gemList.append(GemType[gemIndex])
      if self.displayOn:
        self.drawBank(stdscr, height, width, gemList)

    options = dict()
    for i, gem in enumerate(gemList):
      options['gemType'+str(i+1)] = gem
    return options

  def getDevCardLocAndValidate(self, stdscr, height, width, reserving):
    agent = self.agents[self.gameState.whoseTurn]

    if self.displayOn:
      # Draw options for devCard tier
      devCardTierPollStr = " Possible Tier   |" + \
                           " 1:Tier o"   + " |" + \
                           " 2:Tier oo"  + " |" + \
                           " 3:Tier ooo" + " |"
      stdscr.attron(curses.color_pair(10))
      stdscr.addstr(height-3, 0, devCardTierPollStr)
      stdscr.addstr(height-3, len(devCardTierPollStr), " " * (width - len(devCardTierPollStr) - 1))
      stdscr.attroff(curses.color_pair(10))
    keyPressed = 0
    if agent.isKeyboardAgent:
      keyPressed = stdscr.getch()
    else:
      keyPressed = agent.simKeyStrokes.pop(0)
    # if tier not in options, return None
    if not ( ord('0') < keyPressed < ord('4') ): return None
    tier = keyPressed - ord('0')

    if self.displayOn:
      # Draw options for devCard column
      devCardColPollStr = " Possible Column  |"
      if reserving:
        devCardColPollStr += " 0:DeckTop" + " |"
      devCardColPollStr += " 1:Column 1" + " |" + \
                           " 2:Column 2" + " |" + \
                           " 3:Column 3" + " |" + \
                           " 4:Column 4" + " |"
      stdscr.attron(curses.color_pair(10))
      stdscr.addstr(height-3, 0, devCardColPollStr)
      stdscr.addstr(height-3, len(devCardColPollStr), " " * (width - len(devCardColPollStr) - 1))
      stdscr.attroff(curses.color_pair(10))
    if agent.isKeyboardAgent:
      keyPressed = stdscr.getch()
    else:
      keyPressed = agent.simKeyStrokes.pop(0)
    # if column not in options, return None
    if (not reserving) and not ( ord('0') < keyPressed < ord('5') ): return None
    if reserving and not ( ord('0') <= keyPressed < ord('5') ): return None
    column = keyPressed - ord('0')

    pos = (tier, column)
    return pos

  def askForDevCardLoc(self, stdscr, height, width, reserving=False):
    devCardLoc = self.getDevCardLocAndValidate(stdscr, height, width, reserving)
    if (devCardLoc is None): return None
    options = dict()
    options['devCardLoc'] = devCardLoc # (tier, rowIndex)
    return options

  def getReserveLocAndValidate(self, stdscr, height, width):
    agent = self.agents[self.gameState.whoseTurn]

    if self.displayOn:
      # Draw options for reserve index {1, 2, 3}
      reservePollStr =  " Reserve Index (Non-Empty) |" + \
                        " 1" + " |" + \
                        " 2" + " |" + \
                        " 3" + " |"
      stdscr.attron(curses.color_pair(10))
      stdscr.addstr(height-3, 0, reservePollStr)
      stdscr.addstr(height-3, len(reservePollStr), " " * (width - len(reservePollStr) - 1))
      stdscr.attroff(curses.color_pair(10))
    if agent.isKeyboardAgent:
      keyPressed = stdscr.getch()
    else:
      keyPressed = agent.simKeyStrokes.pop(0)
    # if index not in options, return None
    if not ( ord('0') < keyPressed < ord('4') ): return None
    return keyPressed - ord('0')

  def askForReserveLoc(self, stdscr, height, width):
    reserveLoc = self.getReserveLocAndValidate(stdscr, height, width)
    if (reserveLoc is None): return None
    options = dict()
    options['reserveLoc'] = reserveLoc # (devCardReserves index)
    return options

  def getGemIndexAndValidate(self, stdscr, height, width, discard=False):
    agent = self.agents[self.gameState.whoseTurn]

    if self.displayOn:
      toDiscard = "To Discard" if discard else ""
      # Draw options for Gems Types
      gemTypesPollStr = " Possible Gems {} |".format(toDiscard) + \
                        " 1:" + GemTypesColors[0]  + " |" + \
                        " 2:" + GemTypesColors[1]  + " |" + \
                        " 3:" + GemTypesColors[2]  + " |" + \
                        " 4:" + GemTypesColors[3]  + " |" + \
                        " 5:" + GemTypesColors[4]  + " |"
      colorPair = 8 if discard else 10
      stdscr.attron(curses.color_pair(colorPair))
      stdscr.addstr(height-3, 0, gemTypesPollStr)
      stdscr.addstr(height-3, len(gemTypesPollStr), " " * (width - len(gemTypesPollStr) - 1))
      stdscr.attroff(curses.color_pair(colorPair))

    if agent.isKeyboardAgent:
      keyPressed = stdscr.getch()
    else:
      if discard:
        # If an automated agent ever has to discard, just return what you took.
        # This is a terrible strategy an AI agent should avoid!
        # print "lastSimKeyStrokes={} turn={}".format(agent.lastSimKeyStrokes, self.gameState.whoseTurn)
        if agent.lastSimKeyStrokes[0] == ord('2'):
          keyPressed = agent.lastSimKeyStrokes[1]
          agent.lastSimKeyStrokes[0] = ord('0')
        else:
          keyPressed = agent.lastSimKeyStrokes.pop(1)
      else:
        keyPressed = agent.simKeyStrokes.pop(0)
    # if gem not in options, return None
    if not ( ord('0') < keyPressed < ord('6') ): return None
    # gem
    return keyPressed - ord('0') - 1

  def askFor2EqualGems( self , stdscr, height, width):
    gemIndex = self.getGemIndexAndValidate(stdscr, height, width)
    if (gemIndex is None): return None
    options = dict()
    options['gemType1'] = GemType[gemIndex]
    return options

  def askFor3DiffGems( self , stdscr, height, width):
    gemList = []
    for _ in range(3):
      gemIndex = self.getGemIndexAndValidate(stdscr, height, width)
      # if gem already picked, return None
      if (gemIndex is None) or (GemType[gemIndex] in gemList): return None
      gemList.append(GemType[gemIndex])
      if self.displayOn:
        self.drawBank(stdscr, height, width, gemList)
    options = dict()
    options['gemType1'] = gemList[0]
    options['gemType2'] = gemList[1]
    options['gemType3'] = gemList[2]
    return options

  def startColor( self ):
    curses.start_color()
    # Tokens
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
    curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_BLUE)
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_RED)
    curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_WHITE)
    curses.init_pair(5, curses.COLOR_WHITE, 240)
    # curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_BLACK)
    # Jokers
    curses.init_pair(6, curses.COLOR_BLACK, curses.COLOR_YELLOW)
    # curses.init_pair(6, curses.COLOR_BLACK, 227)
    # Normal Text
    curses.init_pair(7, curses.COLOR_WHITE, curses.COLOR_BLACK)
    # Invalid Action
    curses.init_pair(8, curses.COLOR_RED, curses.COLOR_BLACK)
    # Title
    curses.init_pair(9, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    # Status Bar
    curses.init_pair(10, curses.COLOR_BLACK, curses.COLOR_WHITE)
    # Gems Icons
    curses.init_pair(11, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(12, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(13, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(14, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(15, 240, curses.COLOR_BLACK)
    # curses.init_pair(15, curses.COLOR_BLACK, curses.COLOR_BLACK)

  def drawTitleBorder(self, stdscr, height, width):
    # Add border
    stdscr.border(0)
    title = " SPLENDOR "[:width-1]
    start_x_title = int((width // 2) - (len(title) // 2) - len(title) % 2)
    start_y_title = 0
    # Turning on attributes for title
    stdscr.attron(curses.color_pair(9))
    stdscr.attron(curses.A_BOLD)
    # Rendering title
    stdscr.addstr(start_y_title, start_x_title, title)
    # Turning off attributes for title
    stdscr.attroff(curses.color_pair(9))
    stdscr.attroff(curses.A_BOLD)

  def drawStatusBar(self, stdscr, height, width, keyPressed, text=""):
    if keyPressed == 0: keystr = "No key press detected..."[:width-1]
    keystr = "Last key pressed: {}".format(keyPressed)[:width-1]
    whstr = "Width: {}, Height: {}".format(width, height)
    statusbarstr = " Press ESC to exit | STATUS BAR | " + \
                   "Prestige To Win: {} | ".format(self.gameState.prestigeToWin) + \
                   "Turn: Agent #{} | ".format(self.gameState.whoseTurn) + \
                   "# Rounds: {} | ".format(self.roundsNumber)  + \
                   keystr + " | " + \
                   whstr + " | " + text[:20]
    stdscr.attron(curses.color_pair(6))
    stdscr.addstr(height-1, 0, statusbarstr)
    stdscr.addstr(height-1, len(statusbarstr), " " * (width - len(statusbarstr) - 1))
    stdscr.attroff(curses.color_pair(6))

  def drawPossibleActions(self, stdscr, height, width):
    actionsPollStr = " Possible Actions  |" + \
                     " 1:" + ActionStr[0] + " |" + \
                     " 2:" + ActionStr[1] + " |" + \
                     " 3:" + ActionStr[2] + " |" + \
                     " 4:" + ActionStr[3] + " |" + \
                     " 5:" + ActionStr[4] + " |" + \
                     " 6:" + ActionStr[5] + " |"
    stdscr.attron(curses.color_pair(10))
    stdscr.addstr(height-3, 0, actionsPollStr)
    stdscr.addstr(height-3, len(actionsPollStr), " " * (width - len(actionsPollStr) - 1))
    stdscr.attroff(curses.color_pair(10))

  def drawBank(self, stdscr, height, width, withdrawing=None):
    bank_width = 7 + 1 + 7 + 1 + 7 + 1 + 7 + 1 + 7 + 1 + 7
    bank_height = 4

    bank_x = (width // 2) - (bank_width // 2)
    bank_y = 1
    # Clear bank title string
    stdscr.addstr(bank_y, bank_x, " " * bank_width)

    title = " BANK "
    if withdrawing is not None:
      for gemType in withdrawing:
        title = title + " [{}]".format(gemType)
    title_x = bank_x + (bank_width // 2) - (len(title) // 2) - (len(title) % 2)
    title_y = bank_y
    stdscr.addstr(title_y, title_x, title)

    for i, gemType in enumerate(GemType):
      self.drawBankToken(stdscr, i+1, bank_y+1, bank_x+i*8, gemType, self.gameState.tableState.tokens[i])
    self.drawBankToken(stdscr, 6, bank_y+1, bank_x+40, JokerType[0], self.gameState.tableState.jokers)

  def drawBankToken(self, stdscr, colorPair, offset_y, offset_x, gemType, numberOfTokens):
    token_width = 6
    token_height = 3
    pixelRow = (" ",) * token_width
    tokenPixels = [list(pixelRow) for _ in range(token_height)]
    tokenPixels[1][2] = str(numberOfTokens/10) # decenas
    tokenPixels[1][3] = str(numberOfTokens%10) # unidades
    stdscr.attron(curses.color_pair(colorPair))
    for i in range(len(tokenPixels)):
      for j in range(len(tokenPixels[0])):
        stdscr.addch(offset_y+i, offset_x+j, tokenPixels[i][j])
    stdscr.attroff(curses.color_pair(colorPair))
    stdscr.attron(curses.color_pair(7))
    stdscr.addstr(offset_y+token_height,\
                  offset_x+(token_width//2)-(len(gemType)//2)-(len(gemType)%2),\
                  gemType)
    stdscr.attroff(curses.color_pair(7))

  def drawTableNobleTiles(self, stdscr, height, width):
    tableNobles_width = (1 + 11 + 1) * len(self.gameState.tableState.nobleTiles)
    tableNobles_height = 6

    tableNobles_x = (width // 2) - (tableNobles_width // 2) - (tableNobles_width % 2)
    tableNobles_y = 7

    for i, nobleTile in enumerate(self.gameState.tableState.nobleTiles):
      self.drawTableNobleTile(stdscr, tableNobles_y, tableNobles_x+i*13, nobleTile)

  def drawTableNobleTile(self, stdscr, offset_y, offset_x, nobleTile):
    tableNoble_width = (1 + 11 + 1)
    tableNoble_height = 6

    # Draw noble tile margin
    stdscr.attron(curses.color_pair(7))
    for i in range(tableNoble_height):
      if (0 < i < (tableNoble_height-1)):
        stdscr.addch(offset_y+i, offset_x, curses.ACS_VLINE)
        stdscr.addch(offset_y+i, offset_x+tableNoble_width-1, curses.ACS_VLINE)
    for j in range(tableNoble_width):
      if (0 < j < (tableNoble_width-1)):
        stdscr.addch(offset_y, offset_x+j, curses.ACS_HLINE)
        stdscr.addch(offset_y+tableNoble_height-1, offset_x+j, curses.ACS_HLINE)
    stdscr.addch(offset_y, offset_x, curses.ACS_ULCORNER)
    stdscr.addch(offset_y, offset_x+tableNoble_width-1, curses.ACS_URCORNER)
    stdscr.addch(offset_y+tableNoble_height-1, offset_x, curses.ACS_LLCORNER)
    stdscr.addch(offset_y+tableNoble_height-1, offset_x+tableNoble_width-1, curses.ACS_LRCORNER)
    stdscr.attroff(curses.color_pair(7))

    # Draw Noble Data
    nobleTitle = " NOBLE "
    prestigeStr = "Prestige:{}".format(nobleTile.prestige)
    requirementsStrList = [" {} {}".format(gemCost[0], gemCost[1]) \
                           for gemCost in nobleTile.requirements.gemCostTuplesList]
    requirementsStrList.insert(0, prestigeStr)
    requirementsStrList.insert(0, nobleTitle)
    stdscr.attron(curses.color_pair(7))
    for i, text in enumerate(requirementsStrList):
      stdscr.addstr(offset_y+i, offset_x+1, text)
    stdscr.attroff(curses.color_pair(7))

    # Draw gem icon
    for i in range(len(requirementsStrList)-2):
      colorPair = 11 + GemType.index(nobleTile.requirements.gemCostTuplesList[i][1])
      stdscr.attron(curses.color_pair(colorPair))
      stdscr.addch(offset_y+i+2, offset_x+1, curses.ACS_DIAMOND)
      stdscr.attroff(curses.color_pair(colorPair))


  def drawTableDeckAndDevCards(self, stdscr, height, width):
    tableCards_width = (1 + 13 + 1 + 1) * 5 # 1 deck + 4 devCards face-up.
    tableCards_height = 12 * 3 # 12 per devCard times 3 rows/decks.

    tableCards_x = (width // 2) - (tableCards_width // 2) - (tableCards_width % 2)
    tableCards_y = 14

    # Draw each row/deck tier.
    for i in range(3):
      self.drawTableCardsTier(stdscr, 2-i, tableCards_y+i*12, tableCards_x)

  def drawTableCardsTier(self, stdscr, tier, offset_y, offset_x):
    # Draw deck pile tier
    self.drawTableDeckTier(stdscr, tier, offset_y, offset_x)
    # Draw devCards tier/row. Tier 3 first!
    for i, devCard in enumerate(self.gameState.tableState.devCards[tier]):
      self.drawTableDevCard(stdscr, offset_y, offset_x+(i+1)*(15+1), devCard)

  def drawTableDeckTier(self, stdscr, tier, offset_y, offset_x):
    tableDeckTier_width = (1 + 13 + 1)
    tableDeckTier_height = 12

    # Draw deck tier margin
    stdscr.attron(curses.color_pair(7))
    for i in range(tableDeckTier_height):
      if (0 < i < (tableDeckTier_height-1)):
        stdscr.addch(offset_y+i, offset_x, curses.ACS_VLINE)
        stdscr.addch(offset_y+i, offset_x+tableDeckTier_width-1, curses.ACS_VLINE)
    for j in range(tableDeckTier_width):
      if (0 < j < (tableDeckTier_width-1)):
        stdscr.addch(offset_y, offset_x+j, curses.ACS_HLINE)
        stdscr.addch(offset_y+tableDeckTier_height-1, offset_x+j, curses.ACS_HLINE)
    stdscr.addch(offset_y, offset_x, curses.ACS_ULCORNER)
    stdscr.addch(offset_y, offset_x+tableDeckTier_width-1, curses.ACS_URCORNER)
    stdscr.addch(offset_y+tableDeckTier_height-1, offset_x, curses.ACS_LLCORNER)
    stdscr.addch(offset_y+tableDeckTier_height-1, offset_x+tableDeckTier_width-1, curses.ACS_LRCORNER)
    stdscr.attroff(curses.color_pair(7))

    # Draw Noble Data
    deckTitle = " DECK "
    numCardsInDeckStr = str(len(self.gameState.tableState.decks[tier]))
    tierStr = " TIER "
    tierIcon = "o" * (tier + 1)

    if tier == 2: colorPair = 12
    elif tier == 1: colorPair = 9
    else: colorPair = 11

    stdscr.attron(curses.color_pair(7))
    stdscr.addstr(offset_y, offset_x+1, deckTitle)
    stdscr.attroff(curses.color_pair(7))

    stdscr.attron(curses.color_pair(colorPair))
    stdscr.attron(curses.A_BOLD)
    x = offset_x + tableDeckTier_width // 2
    stdscr.addstr(offset_y+4, x-(len(numCardsInDeckStr)//2)-(len(numCardsInDeckStr)%2), numCardsInDeckStr)
    stdscr.addstr(offset_y+tableDeckTier_height-3, x-(len(tierStr)//2)-(len(tierStr)%2), tierStr)
    stdscr.addstr(offset_y+tableDeckTier_height-2, x-(len(tierIcon)//2)-(len(tierIcon)%2), tierIcon)
    stdscr.attroff(curses.color_pair(colorPair))
    stdscr.attroff(curses.A_BOLD)


  def drawTableDevCard(self, stdscr, offset_y, offset_x, devCard):
    tableDevCard_width = (1 + 13 + 1)
    tableDevCard_height = 12

    # Draw devCard margin
    stdscr.attron(curses.color_pair(7))
    for i in range(tableDevCard_height):
      if (0 < i < (tableDevCard_height-1)):
        stdscr.addch(offset_y+i, offset_x, curses.ACS_VLINE)
        stdscr.addch(offset_y+i, offset_x+tableDevCard_width-1, curses.ACS_VLINE)
    for j in range(tableDevCard_width):
      if (0 < j < (tableDevCard_width-1)):
        stdscr.addch(offset_y, offset_x+j, curses.ACS_HLINE)
        stdscr.addch(offset_y+tableDevCard_height-1, offset_x+j, curses.ACS_HLINE)
    stdscr.addch(offset_y, offset_x, curses.ACS_ULCORNER)
    stdscr.addch(offset_y, offset_x+tableDevCard_width-1, curses.ACS_URCORNER)
    stdscr.addch(offset_y+tableDevCard_height-1, offset_x, curses.ACS_LLCORNER)
    stdscr.addch(offset_y+tableDevCard_height-1, offset_x+tableDevCard_width-1, curses.ACS_LRCORNER)
    stdscr.attroff(curses.color_pair(7))

    # Draw devCard Data
    textList = []
    if devCard is None:
      textList.append(" EMPTY ")
    else:
      textList.append(" DEV CARD ")
      textList.append("Prestige: {}".format(devCard.prestige))
    stdscr.attron(curses.color_pair(7))
    for i, text in enumerate(textList):
      stdscr.addstr(offset_y+i, offset_x+1, text)

    costStrList = []
    if devCard is not None:
      costStrList = [" {} {}".format(gemCost[0], gemCost[1]) \
                     for gemCost in devCard.cost.gemCostTuplesList]
    while len(costStrList) < 5: costStrList.insert(0, "")
    for i, text in enumerate(costStrList):
      stdscr.addstr(offset_y+6+i, offset_x+1, text)

    if devCard is not None:
      # Draw devCard gem type
      stdscr.addstr(offset_y+3, offset_x+6, devCard.gemType)
      stdscr.attroff(curses.color_pair(7))

      # Draw devCard gem icon
      for i in range(3):
        for j in range(4):
          if i == 0 and j == 0: continue
          if i == 2 and j == 3: continue
          if i == 0 and j == 3: continue
          if i == 2 and j == 0: continue
          colorPair = 11 + GemType.index(devCard.gemType)
          char = curses.ACS_CKBOARD if (i==1) and (0<j<3) else curses.ACS_DIAMOND
          stdscr.attron(curses.color_pair(colorPair))
          stdscr.addch(offset_y+2+i, offset_x+1+j, char)
          stdscr.attroff(curses.color_pair(colorPair))

      # Draw cost gem icons
      indexStart = 0
      for i, costStr in enumerate(costStrList):
        if costStr == "":
          indexStart = i+1
          continue
        colorPair = 11 + GemType.index(devCard.cost.gemCostTuplesList[i-indexStart][1])
        stdscr.attron(curses.color_pair(colorPair))
        stdscr.addch(offset_y+6+i, offset_x+1, curses.ACS_DIAMOND)
        stdscr.attroff(curses.color_pair(colorPair))

  def drawAgentReserve(self, stdscr, offset_y, offset_x, devCardReserve):
    agentReserve_width = (1 + 8 + 1)
    agentReserve_height = 7

    # Draw devCardReserve margin
    stdscr.attron(curses.color_pair(7))
    for i in range(agentReserve_height):
      if (0 < i < (agentReserve_height-1)):
        stdscr.addch(offset_y+i, offset_x, curses.ACS_VLINE)
        stdscr.addch(offset_y+i, offset_x+agentReserve_width-1, curses.ACS_VLINE)
    for j in range(agentReserve_width):
      if (0 < j < (agentReserve_width-1)):
        stdscr.addch(offset_y, offset_x+j, curses.ACS_HLINE)
        stdscr.addch(offset_y+agentReserve_height-1, offset_x+j, curses.ACS_HLINE)
    stdscr.addch(offset_y, offset_x, curses.ACS_ULCORNER)
    stdscr.addch(offset_y, offset_x+agentReserve_width-1, curses.ACS_URCORNER)
    stdscr.addch(offset_y+agentReserve_height-1, offset_x, curses.ACS_LLCORNER)
    stdscr.addch(offset_y+agentReserve_height-1, offset_x+agentReserve_width-1, curses.ACS_LRCORNER)
    stdscr.attroff(curses.color_pair(7))

    # Draw devCardReserve Data
    cardTitle = ""
    if devCardReserve == None:
      cardTitle = " EMPTY "
    else:
      cardTitle = " R: P:{} ".format(devCardReserve.prestige)
    stdscr.attron(curses.color_pair(7))
    stdscr.addstr(offset_y, offset_x+1, cardTitle)

    costStrList = []
    if devCardReserve is not None:
      costStrList = [" {} {}".format(gemCost[0], gemCost[1][:3]) \
                     for gemCost in devCardReserve.cost.gemCostTuplesList]
    while len(costStrList) < 5: costStrList.insert(0, "")
    for i, text in enumerate(costStrList):
      stdscr.addstr(offset_y+1+i, offset_x+1, text)
    stdscr.attroff(curses.color_pair(7))

    # Draw cost gem icons
    indexStart = 0
    for i, costStr in enumerate(costStrList):
      if costStr == "":
        indexStart = i+1
        continue
      colorPair = 11 + GemType.index(devCardReserve.cost.gemCostTuplesList[i-indexStart][1])
      stdscr.attron(curses.color_pair(colorPair))
      stdscr.addch(offset_y+1+i, offset_x+1, curses.ACS_DIAMOND)
      stdscr.attroff(curses.color_pair(colorPair))

    if devCardReserve is not None:
      # Draw devCardReserve gem icon
      colorPair = 11 + GemType.index(devCardReserve.gemType)
      stdscr.attron(curses.color_pair(colorPair))
      stdscr.addch(offset_y, offset_x+4, curses.ACS_DIAMOND)
      stdscr.attroff(curses.color_pair(colorPair))


  def drawAgentStates(self, stdscr, height, width, agentIndexInTurn):
    agentState_width = 4 + 1 + 4 + 1 + 4 + 1 + 4 + 1 + 4 + 1 + 4
    agentState_height = 2 + 1 + 4 + 1 + 4
    for agentIndex in range(len(self.agents)):
      inTurn = (agentIndexInTurn == agentIndex)
      if agentIndex == 0:
        x = 2
        y = 2
      elif agentIndex == 2:
        x = 2
        y = (height // 2)
      elif agentIndex == 1:
        x = width - 3 - agentState_width
        y = 2
      elif agentIndex == 3:
        x = width - 3 - agentState_width
        y = (height // 2)
      self.drawAgentState(stdscr, y, x, agentState_height, agentState_width, agentIndex, inTurn)

  def drawAgentState(self, stdscr, y, x, height, width, agentIndex, inTurn):
    agent = self.gameState.agentStates[agentIndex]
    totalTokens = sum(agent.tokens) + agent.jokers
    title = "* " if inTurn else ""
    title += "PLAYER:{} Prestige:{}".format(agentIndex, agent.getPrestige())
    title_x = x + (width // 2) - (len(title) // 2) - (len(title) % 2)
    title_y = y
    stdscr.addstr(title_y, title_x, title)

    # Draw agent tokens
    for i, gemType in enumerate(GemType):
      self.drawAgentToken(stdscr, i+1, y+1, x+i*5, gemType, agent.tokens[i])
    self.drawAgentToken(stdscr, 6, y+1, x+25, JokerType[0], agent.jokers)

    # Draw agent devCard bonuses
    for i, gemType in enumerate(GemType):
      numberOfGemsCards = sum([1 if (devCard.gemType == gemType) else 0 for devCard in agent.devCards])
      self.drawAgentCard(stdscr, 11+GemType.index(gemType), y+4, x+i*5, gemType, numberOfGemsCards)

    # Draw stats: number of tokens+jokers, and number of reserves.
    stat_x = x + width - 4
    stat_y = y + 5
    statNumTokens = str(totalTokens)
    statNumNobles = str(len(agent.nobleTiles))
    stdscr.attron(curses.color_pair(7))
    stdscr.addstr(stat_y, stat_x, "T:{}".format(statNumTokens))
    stdscr.addstr(stat_y+1, stat_x, "N:{}".format(statNumNobles))
    stdscr.attroff(curses.color_pair(7))

    # Draw agent reserved devCards
    for i in range(3):
      if i < len(agent.devCardReserves):
        devCardReserve = agent.devCardReserves[i]
      else:
        devCardReserve = None
      self.drawAgentReserve(stdscr, y+8, x+i*10, devCardReserve)

  def drawAgentToken(self, stdscr, colorPair, offset_y, offset_x, gemType, numberOfTokens):
    token_width = 4
    token_height = 2
    numStr = str(numberOfTokens)
    pixelRow = (" ",) * token_width
    tokenPixels = [list(pixelRow) for _ in range(token_height)]
    stdscr.attron(curses.color_pair(colorPair))
    for i in range(len(tokenPixels)):
      for j in range(len(tokenPixels[0])):
        stdscr.addch(offset_y+i, offset_x+j, tokenPixels[i][j])
    stdscr.attroff(curses.color_pair(colorPair))
    stdscr.attron(curses.color_pair(7))
    stdscr.addstr(offset_y+token_height,\
                  offset_x+(token_width//2)-(len(numStr)//2)-(len(numStr)%2),\
                  numStr)
    stdscr.attroff(curses.color_pair(7))

  def drawAgentCard(self, stdscr, colorPair, offset_y, offset_x, gemType, numberOfGemsCards):
    gem_width = 4
    gem_height = 3
    numStr = str(numberOfGemsCards)

    # Draw devCard gem icon
    for i in range(gem_height):
      for j in range(gem_width):
        if i == 0 and j == 0: continue
        if i == 2 and j == 3: continue
        if i == 0 and j == 3: continue
        if i == 2 and j == 0: continue
        char = curses.ACS_CKBOARD if (i==1) and (0<j<3) else curses.ACS_DIAMOND
        stdscr.attron(curses.color_pair(colorPair))
        stdscr.addch(offset_y+i, offset_x+j, char)
        stdscr.attroff(curses.color_pair(colorPair))

    # Draw number of gem devCards
    stdscr.attron(curses.color_pair(7))
    stdscr.addstr(offset_y+gem_height,\
                  offset_x+(gem_width//2)-(len(numStr)//2)-(len(numStr)%2),\
                  numStr)
    stdscr.attroff(curses.color_pair(7))

  def drawInvalidActionMsg(self, stdscr, height, width):
    msg = " !!! INVALID ACTION !!! "[:width-1]
    start_x_msg = int((width // 2) - (len(msg) // 2) - len(msg) % 2)
    start_y_msg = 0
    # Turning on attributes for msg
    stdscr.attron(curses.color_pair(8))
    stdscr.attron(curses.A_BOLD)
    # Rendering msg
    stdscr.addstr(start_y_msg, start_x_msg, msg)
    # Turning off attributes for msg
    stdscr.attroff(curses.color_pair(8))
    stdscr.attroff(curses.A_BOLD)



class GameRules:

  def canBuyDevCard(self, gameState, agentIndex, devCard):
    if devCard is None: return False
    # Check if agent buyer has enough gem cash.
    agent = gameState.agentStates[agentIndex]
    isItForFree = self.isItForFree(gameState, devCard.cost)
    remainingTokensToCharge = None
    if isItForFree:
      # 5 gem Types + jokers
      remainingTokensToCharge = (0,0,0,0,0,0)
    else:
      # If cant afford, returns None
      remainingTokensToCharge = self.getTokensToCharge(gameState, devCard.cost)

    if not (isItForFree or remainingTokensToCharge != None): return False

    return True


  def applyAction(self, action, options, gameState):
    if action == Actions.Take3DiffGems:
      return self.grantThreeTokens(gameState, options['gemType1'], \
                                              options['gemType2'], \
                                              options['gemType3']  )
    elif action == Actions.Take2EqualGems:
      return self.grantTwoTokens(gameState, options['gemType1'])
    elif action == Actions.ReserveDevCard:
      return self.devCardReserve(gameState, options['devCardLoc'])
    elif action == Actions.BuyDevCard:
      return self.devCardPurchase(gameState, options['devCardLoc'])
    elif action == Actions.BuyReservedDevCard:
      return self.reservePurchase(gameState, options['reserveLoc'])
    elif action == Actions.NoOp:
      return True
    elif action == Actions.DiscardTokens:
      return self.discardTokens(gameState, options)
    else:
      print "Unknown action!"
    return False


  def discardTokens(self, gameState, options):
    agentIndex = gameState.whoseTurn
    currentTokens = list(gameState.tableState.tokens)
    agentTokens = list(gameState.agentStates[agentIndex].tokens)
    for key,gemType in options.items():
      gemIndex = GemType.index(gemType)
      agentTokens[gemIndex] -= 1
      currentTokens[gemIndex] += 1
    gameState.tableState.tokens = tuple(currentTokens)
    gameState.agentStates[agentIndex].tokens = tuple(agentTokens)
    return True

  def isItForFree(self, gameState, cost, agentIndex=-1):
    if agentIndex != -1:
      agent = gameState.agentStates[agentIndex]
    else:
      agent = gameState.agentStates[gameState.whoseTurn]
    for gemCost in cost.gemCostTuplesList:
      gemDevCardCount = sum([1 if (gemCost[1] == devCard.gemType) else 0 for devCard in agent.devCards])
      if gemCost[0] > gemDevCardCount:
        return False
    return True

  def chargeAgent(self, gameState, tokensTuple):
    agent = gameState.agentStates[gameState.whoseTurn]
    table = gameState.tableState
    agentTokens = list(agent.tokens)
    tableTokens = list(table.tokens)
    for i in range(len(tokensTuple)-1):
      agentTokens[i] -= tokensTuple[i]
      tableTokens[i] += tokensTuple[i]
    gameState.agentStates[gameState.whoseTurn].tokens = tuple(agentTokens)
    gameState.tableState.tokens = tuple(tableTokens)

    # Charge any number of jokers
    jokersToCharge = tokensTuple[-1]
    agent.jokers -= jokersToCharge
    table.jokers += jokersToCharge

  def getTokensToCharge(self, gameState, cost, agentIndex=-1):
    if agentIndex != -1:
      agent = gameState.agentStates[agentIndex]
    else:
      agent = gameState.agentStates[gameState.whoseTurn]
    retval = [0,0,0,0,0,0]
    agentJokers = agent.jokers
    # If can't afford, return None
    gemCostList = list(cost.gemCostTuplesList)
    for gemCost in gemCostList:
      gemDevCardCount = sum([1 if (gemCost[1] == devCard.gemType) else 0 for devCard in agent.devCards])
      diff = gemCost[0] - gemDevCardCount
      if diff <= 0: continue # this is free.
      gemIndex = GemType.index(gemCost[1])
      if diff <= agent.tokens[gemIndex]:
        # Afford without using jokers.
        retval[gemIndex] = diff
      elif diff <= (agent.tokens[gemIndex] + agentJokers):
        # Afford with all tokens of this gemType, and...
        retval[gemIndex] = agent.tokens[gemIndex]
        # Use jokers for the extra
        retval[-1] += (diff - agent.tokens[gemIndex])
        agentJokers -= (diff - agent.tokens[gemIndex])
      else:
        # Even if we use jokers? ok...
        # Can't afford this, get out.
        return None

    return tuple(retval)

  def drawDevCardFromDeck(self, gameState, tier):
    deck = gameState.tableState.decks[tier]
    if len(deck) == 0: return None
    # Updates deck and returns next card.
    return deck.pop(0)

  def devCardReserve(self, gameState, devCardLoc):
    # Can assume |devCardLoc|=(tier, rowIndex) is a valid location.
    # if rowIndex == 0 --> draw deckTop card.

    # Check if agent has less than 3 reserves. Else this is not a valid action.
    agent = gameState.agentStates[gameState.whoseTurn]
    table = gameState.tableState
    if len(agent.devCardReserves) >= 3:
      return False

    # Reserving from deckTop.
    if devCardLoc[1] == 0:
      tier = devCardLoc[0]-1
      # Check if deck tier not empty.
      devCard = self.drawDevCardFromDeck(gameState, tier)
      if devCard is None: return False

      # Update agentState with new reserve drawn from deck.
      agent.devCardReserves.append(devCard)

    # Reserving from table face-up devCards.
    else:
      tier = devCardLoc[0]-1
      index = devCardLoc[1]-1
      # Check if card exists at location.
      devCard = table.devCards[tier][index]
      if devCard is None: return False

      # Update agentState with new reserve.
      agent.devCardReserves.append(devCard)

      # After the agentState is updated, now we need to draw a new devCard
      # from the same deck tier to replace it in the same devCardLoc.
      # if deck tier is empty, drawDevCardFromDeck returns a None devCard.
      # TODO: We must ensure this is correctly displayed too!
      table.devCards[tier][index] = self.drawDevCardFromDeck(gameState, tier)

    # To grant a reservation Gold Token a.k.a. Joker we must:
    # Check if agent has less than 10 tokens+jokers and there are table jokers available.
    # This joker grant is not needed for a reservation to occur - is more like a bonus.
    totalTokens = sum(agent.tokens) + agent.jokers
    if (table.jokers > 0) and (totalTokens < 10):
      # Grant Gold Token / Joker!
      table.jokers -= 1
      agent.jokers += 1

    return True


  def getAgentNumberOfGemTypeCards(self, agentState, gemIndex):
    retval = 0
    for devCard in agentState.devCards:
      if gemIndex == GemType.index(devCard.gemType):
        retval += 1
    return retval


  def checkNobleRequirements(self, gameState):
    agent = gameState.agentStates[gameState.whoseTurn]
    table = gameState.tableState

    nobleVisitorsIndexes = []
    for i,nobleTile in enumerate(table.nobleTiles):
      requirementMet = True
      for gemCost in nobleTile.requirements.gemCostTuplesList:
        costNumber = gemCost[0]
        gemType = gemCost[1]
        gemIndex = GemType.index(gemType)
        agentNumber = self.getAgentNumberOfGemTypeCards(agent, gemIndex)
        if agentNumber < costNumber:
          requirementMet = False
          break
      if requirementMet:
        nobleVisitorsIndexes.append(i)

    if len(nobleVisitorsIndexes) == 0:
      return
    else:
      nobleVisitorsIndexes.sort(reverse = True)
      for nobleTileIndex in nobleVisitorsIndexes:
        # Remove nobleTile from tableState and add it to agentState.
        agent.nobleTiles.append(table.nobleTiles.pop(nobleTileIndex))


  def devCardPurchase(self, gameState, devCardLoc):
    # Can assume |devCardLoc|=(tier, rowIndex) is a valid location.
    tier = devCardLoc[0]-1
    index = devCardLoc[1]-1
    # Check if card exists at location.
    devCard = gameState.tableState.devCards[tier][index]
    if devCard is None: return False
    # Check if agent buyer has enough gem cash.
    isItForFree = self.isItForFree(gameState, devCard.cost)
    remainingTokensToCharge = None
    if isItForFree:
      # 5 gem Types + jokers
      remainingTokensToCharge = (0,0,0,0,0,0)
    else:
      # If cant afford, returns None
      remainingTokensToCharge = self.getTokensToCharge(gameState, devCard.cost)

    if not (isItForFree or remainingTokensToCharge != None):
      return False
    # At this point, we can complete the purchase. Charge agent.
    self.chargeAgent(gameState, remainingTokensToCharge)
    agentIndex = gameState.whoseTurn
    gameState.agentStates[agentIndex].devCards.append(devCard)
    # replace the now empty card slot/pos with new card drawn from deck tier pile (can be None if empty deck).
    gameState.tableState.devCards[tier][index] = self.drawDevCardFromDeck(gameState, tier)

    # Before returning, check if agent/player reached prestige requirements for a Noble visit.
    self.checkNobleRequirements(gameState)

    return True


  def reservePurchase(self, gameState, reserveLoc):
    # reserveLoc is an index in range [1,3] inclusively.
    resIndex = reserveLoc - 1
    # Check if reserve index exists!
    agent = gameState.agentStates[gameState.whoseTurn]
    if resIndex >= len(agent.devCardReserves):
      return False
    devCard = agent.devCardReserves[resIndex]
    # Check if agent buyer has enough gem cash.
    isItForFree = self.isItForFree(gameState, devCard.cost)
    remainingTokensToCharge = None
    if isItForFree:
      # 5 gem Types + jokers
      remainingTokensToCharge = (0,0,0,0,0,0)
    else:
      # If cant afford, returns None
      remainingTokensToCharge = self.getTokensToCharge(gameState, devCard.cost)

    if not (isItForFree or remainingTokensToCharge != None):
      return False
    # At this point, we can complete the purchase. Charge agent.
    self.chargeAgent(gameState, remainingTokensToCharge)
    agent.devCards.append(devCard)
    # Remove devCard from list of reserves.
    agent.devCardReserves.pop(resIndex)

    # Before returning, check if agent/player reached prestige requirements for a Noble visit.
    self.checkNobleRequirements(gameState)

    return True


  def grantThreeTokens(self, gameState, gemType1, gemType2, gemType3):
    # Check if possible under the game rules:
    if (gemType1 == gemType2) or (gemType1 == gemType3) or (gemType2 == gemType3):
      print 'Game Rule violation: When taking 3 gems, all must be different!'
      return False

    gemIndexes = []
    gemIndexes.append( GemType.index(gemType1) )
    gemIndexes.append( GemType.index(gemType2) )
    gemIndexes.append( GemType.index(gemType3) )
    numberGemTypesAvailable = sum([1 if gemTypeToken != 0 else 0 for gemTypeToken in gameState.tableState.tokens])

    if numberGemTypesAvailable == 0:
      print 'Game Rule violation: When taking 3 gems, at least 1 must be available in the table!'
      return False

    # Less than 3 gemTypes available: allow for less gems.
    if numberGemTypesAvailable < 3:
      tmpIndexes = []
      for index in gemIndexes:
        if gameState.tableState.tokens[index] != 0:
          tmpIndexes.append(index)
      gemIndexes = tmpIndexes
      if len(gemIndexes) == 0:
          print "Gem Rule violation: When taking 3 gems but having less than 3 available, at least 1 choice must be available!"
          return False

    # At least 3 gemTypes available
    else:
      for index in gemIndexes:
        if gameState.tableState.tokens[index] == 0:
          print 'Game Rule violation: When taking 3 gems, all must be available!'
          return False

    # Remove tokens from table and add tokens to agentState
    agentIndex = gameState.whoseTurn
    currentTokens = list(gameState.tableState.tokens)
    agentTokens = list(gameState.agentStates[agentIndex].tokens)
    for index in gemIndexes:
      currentTokens[index] -= 1
      agentTokens[index] += 1
    gameState.tableState.tokens = tuple(currentTokens)
    gameState.agentStates[agentIndex].tokens = tuple(agentTokens)

    return True


  def grantTwoTokens(self, gameState, gemType1):
    # Check if possible under the game rules:
    gemIndex = GemType.index(gemType1)
    if gameState.tableState.tokens[gemIndex] < 4:
      print 'Game Rule violation: When taking 2 gems, must have at least 4 available!'
      return False

    # Remove tokens from table and add tokens to agentState
    agentIndex = gameState.whoseTurn
    currentTokens = list(gameState.tableState.tokens)
    agentTokens = list(gameState.agentStates[agentIndex].tokens)
    currentTokens[gemIndex] -= 2
    agentTokens[gemIndex] += 2
    gameState.tableState.tokens = tuple(currentTokens)
    gameState.agentStates[agentIndex].tokens = tuple(agentTokens)

    return True