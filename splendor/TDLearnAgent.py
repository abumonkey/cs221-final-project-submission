import random
from game import Agent
import cPickle
import learner
import os, util
import collections

class TDLearnAgent( Agent ):

  def __init__(self, index, epsilon=0.1):
    self.index = index # agent/player index {0,1,2,3}
    self.isKeyboardAgent = False
    self.simKeyStrokes = []
    self.lastSimKeyStrokes = []
    self.epsilonGreedy = epsilon

  def getAgentName(self):
    return "TDLearnAgent"

  def argMax(self, gameState, legalMoves, weightVector):
    x = random.random()
    if x < self.epsilonGreedy:
      return random.sample(legalMoves, 1)[0]

    maxAction = None
    maxValue = float('-inf')
    for legalMove in legalMoves:
      succGameState = gameState.generateSuccessorNoChance(self.index, legalMove)
      phi_succGameState = learner.featureExtractor(succGameState)
      value = util.dotProduct(weightVector, phi_succGameState)
      if value > maxValue:
        maxValue = value
        maxAction = legalMove
    return maxAction

  def argMin(self, gameState, legalMoves, weightVector):
    x = random.random()
    if x < self.epsilonGreedy:
      return random.sample(legalMoves, 1)[0]

    minAction = None
    minValue = float('+inf')
    for legalMove in legalMoves:
      succGameState = gameState.generateSuccessorNoChance(self.index, legalMove)
      phi_succGameState = learner.featureExtractor(succGameState)
      value = util.dotProduct(weightVector, phi_succGameState)
      if value < minValue:
        minValue = value
        minAction = legalMove
    return minAction

  def getAction(self, gameState):
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions(self.index)
    if len(legalMoves) == 0:
      action =  None
    else:
      # Choose policy action
      filename = 'weights'
      if os.path.isfile(filename):
        f = open(filename, "r")
        try: data = cPickle.load(f)
        finally: f.close()
        weightVector = data['weights']
      # No previous experience. Load all Zeros.
      else:
        weightVector = collections.defaultdict(float)

      if self.index % 2 == 0:
        action = self.argMax(gameState, legalMoves, weightVector)
      if self.index % 2 == 1:
        action = self.argMin(gameState, legalMoves, weightVector)

    self.simKeyStrokes = action
    self.lastSimKeyStrokes = list(action)
    return action