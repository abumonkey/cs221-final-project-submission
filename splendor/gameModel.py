import util, math, random, splendorUtil
from collections import defaultdict
from pprint import pprint

############################################################
# MDP model of Splendor with a single opponent as part of
# the environment randomness.

class SplendorMDP(util.MDP):

    # default definition parameters for a 2-player Splendor game.
    # Really a single-player with the opponent abstracted inside the random environment.
    def __init__(self, discount=1, prestigeThreshold=15, currencyMultiplicity=4, numberOfJokers=5, numberOfNobles=3):
        """
        + discount: MDP discoutn factor in range [0,1]. Defaults to 1.
        + prestigeThreshold: maximum number of prestige points are required to end the game in
          the current round. Last round must be completed. Defaults to 15.
        + currencyMultiplicity: number of tokens of each gem color in the game economy. Defaults to 7.
        + numberOfJokers: number of gold joker (yellow) tokens. Defaults to 5.
        """
        self.discount = discount
        self.prestigeThreshold = prestigeThreshold
        self.currencyMultiplicity = currencyMultiplicity
        self.numberOfJokers = numberOfJokers
        self.deckReference = splendorUtil.generateDeck()
        self.numberOfNobles = numberOfNobles

    # Return the start state. (Agent vs Opp)
    # Example of state representation for the Splendor game as MDP.
    # Each state is a tuple with these elements:
    #   -- Player turn: 1:Agent, 0:Opponent
    #   -- Agent chips: (1,2,4,0,1) --> counts on GemTypesColors.
    #   -- Agent jokers: number of jokers.
    #   -- Agent cards: (index1,index2,...) --> unique ids ~ indexes in deck
    #   -- Agent nobles: (index1,index2,index3) --> subset 0 to 3 elements of self.noblesIndex
    #   -- Agent card reserves: (index1,index2,index3) --> unique ids ~ indexes in deck. 0 to 3
    #   -- Opp chips: (1,2,4,0,1) --> counts on GemTypesColors.
    #   -- Opp jokers: number of jokers.
    #   -- Opp cards: (index1,index2,...) --> unique ids ~ indexes in deck
    #   -- Opp nobles: (index1,index2,index3) --> subset 0 to 3 elements of self.noblesIndex
    #   -- Opp card reserves:  (index1,index2,index3) --> unique ids ~ indexes in deck. 0 to 3
    #   -- Table chips: (1,2,4,0,1) --> counts on GemTypesColors.
    #   -- Table jokers: number of jokers.
    #   -- Table cards: 1 row per level.
    #      ((index0,index1,index2,index3), --> Level 3 indexes [70:90]
    #       (index0,index1,index2,index3), --> Level 2 indexes [40:70]
    #       (index0,index1,index2,index3)) --> Level 1 indexes [0:40]
    #   -- Table nobles: (index1,index2,index3) -> subset 0 to 3 elements of self.noblesIndex
    #   -- Table deck: (0,1,0,1,1,0,0,1,...,1,0) indicator function of each card still in the deck.
    #   --  0: card at index no longer in deck. Must be in either player control or in the game cards set.
    #   --  1: card at index is still in deck.
    def startState(self):
        playerTurn = random.randrange(2) # 1 or 0
        agentChips = (0,0,0,0,0)
        agentJokers = 0
        agentCards = None
        agentNobles = None
        agentReserves = None
        oppChips = (0,0,0,0,0)
        oppJokers = 0
        oppCards = None
        oppNobles = None
        oppReserves = None
        tableDeck = (1,) * len(self.deckReference) # Initialize all cards in deck with a presence indicator function.
        tableChips = (self.currencyMultiplicity,) * len(splendorUtil.GemTypesColors) # Initialize Bank
        tableJokers = self.numberOfJokers
        tableNobles = splendorUtil.setupTableNobles(self.numberOfNobles)
        tableDeck = list(tableDeck) # modify deck
        tableCards = splendorUtil.setupTableCards(tableDeck)
        tableDeck = tuple(tableDeck)
        return (playerTurn, \
                agentChips, agentJokers, agentCards, agentNobles, agentReserves, \
                oppChips, oppJokers, oppCards, oppNobles, oppReserves, \
                tableChips, tableJokers, tableCards, tableNobles, tableDeck )

    # Return set of actions possible from |state|.
    # No need to modify this function.
    # All logic for dealing with end states should be placed into the succAndProbReward function below.
    def actions(self, state):
        actions = []
        agentTurn = state[0]
        # Is Agent's turn?
        if agentTurn:
            # Validate 'Bank' actions.
            # Validate 'Buy' actions.

            # Validate 'Reserve' actions.
            agentReserves = state[5]
            if (agentReserves == None) or (len(agentReserves) < 3):
                # We can reserve.
                # Add an action for each card we can reserve in tableCards
                # Add an action for each card we can reserve by drawing from the tableDeck (really 3 decks!)
                for level in range(3):
                    actions.append('Reserve from Level {} Deck'.format(level+1))
                    for column in range(4):
                        actions.append('Reserve from Level {} Column {}'.format(level+1, column))

        else:
            actions.append('WaitForTurn')
        return actions

    # Return a list of (newState, prob, reward) tuples corresponding to edges
    # coming out of |state|.
    # Mapping to notation from class:
    #   state = s, action = a, newState = s', prob = T(s, a, s'), reward = Reward(s, a, s')
    # If IsEnd(state), return the empty list.
    def succAndProbReward(self, state, action): raise NotImplementedError("Override me")

    def discount(self):
        return self.discount

# run some quick tests
print "SPLENDOR!"
splendorMDP = SplendorMDP()
#print "Deck Reference = "
#pprint(splendorMDP.deckReference)
startState = splendorMDP.startState()
print "startState = \n", startState
d = defaultdict(float)
d[startState] = 'Buy'
print d[startState]

#print "NobleTilesAll = "
#pprint(splendorUtil.NobleTilesAll)
splendorUtil.displayNobleTile(splendorUtil.NobleTilesAll[0])

deck = splendorMDP.deckReference
#print "Deck = "
#pprint(deck)
splendorUtil.displayDevCard(deck[89])
print "Deck length = ", len(deck)

print "actions = ", splendorMDP.actions(startState)