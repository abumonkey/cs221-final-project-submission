from game import Agent, Actions
from splendorUtil import GemType
import msvcrt, sys
import learner, util
from collections import defaultdict

class QLearningAgent( Agent ):

  def __init__(self, index, weightVector, discount, featureExtractor, explorationProb=0.2):
    self.index = index # agent/player index {0,1,2,3}
    self.isKeyboardAgent = False
    self.simKeyStrokes = []
    self.lastSimKeyStrokes = []
    self.discount = discount
    self.featureExtractor = featureExtractor
    self.explorationProb = explorationProb
    self.weights = weightVector
    self.numIters = 0
    self.isQLearning = True

  def getAgentName(self):
    return "QLearningAgent"

  # Return the Q function associated with the weights and features
  def getQ(self, state, action):
    score = 0
    for f, v in self.featureExtractor(state, action):
      score += self.weights[f] * v
    return score

  # This algorithm will produce an action given a state.
  # Here we use the epsilon-greedy algorithm: with probability
  # |explorationProb|, take a random action.
  def getAction(self, state):
    self.numIters += 1
    actions = state.getLegalActions(self.index)
    if random.random() < self.explorationProb:
      chosen = random.choice(actions)
    else:
      chosen = max((self.getQ(state, action), action) for action in actions)[1]
    # Remember action picked
    self.simKeyStrokes = chosen
    self.lastSimKeyStrokes = list(chosen)
    return chosen

  # Call this function to get the step size to update the weights.
  def getStepSize(self):
    return 1.0 / math.sqrt(self.numIters)

  # We will call this function with (s, a, r, s'), which you should use to update |weights|.
  # Note that if s is a terminal state, then s' will be None.  Remember to check for this.
  # You should update the weights using self.getStepSize(); use
  # self.getQ() to compute the current estimate of the parameters.
  def incorporateFeedback(self, state, action, reward, newState):
    # Check if state is an end state with newState = None
    if newState == None:
      vopt_s_prime = 0
    else:
      newActions = newState.getLegalActions(self.index)
      vopt_s_prime = max(self.getQ(newState, newAction) for newAction in newActions)
    # Determine Prediction = Q.opt(s, a; w)
    prediction = self.getQ(state, action)
    # Determine Target
    target = reward + (self.discount * vopt_s_prime)
    # Update weights with Standard Q-Learning update.
    for key, value in self.featureExtractor(state, action):
      self.weights[key] -= self.getStepSize()*(prediction - target)*value

# Return a single-element list containing a binary (indicator) feature
# for the existence of the (state, action) pair.  Provides no generalization.
def identityFeatureExtractor(state, action):
    featureKey = (state, action)
    featureValue = 1
    return [(featureKey, featureValue)]