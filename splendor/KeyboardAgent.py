from game import Agent

class KeyboardAgent( Agent ):

  def __init__(self, index):
    self.index = index # agent/player index {0,1,2,3}
    self.isKeyboardAgent = True

  def getAgentName(self):
    return "KeyboardAgent"

  def getAction(self, gameState):
    return None