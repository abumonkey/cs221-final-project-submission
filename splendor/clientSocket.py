# Basic socket communication - Client
from multiprocessing.connection import Client

address = ('localhost', 6000)
conn = Client(address, authkey='splendor')
conn.send('hi!')
# can also send arbitrary objects:
conn.send(['a', 2.5, None, int, sum])
conn.send('close')
conn.close()