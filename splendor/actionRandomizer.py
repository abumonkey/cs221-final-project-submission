import random, splendorUtil
from pprint import pprint

print "Randomized action!"

actions = []

for level in range(3):
    actions.append('Reserve from Level {} Deck'.format(level+1))
    for column in range(4):
        actions.append('Reserve from Level {} Column {}'.format(level+1, column))

for n in range(2):
    if n == 0: # Bank 2 tokens same color
        # Choose 1 random color:
        gemType = random.sample(splendorUtil.GemTypesColors, 1)
        actions.append('Bank {} tokens: {} {} else next color'.format(n+2, gemType, gemType))
    elif n == 1: # Bank 3 tokens same color
        gemType = random.sample(splendorUtil.GemTypesColors, 1)
        gemTypeIndex  = splendorUtil.GemTypesColors.index(gemType[0])
        actions.append('Bank {} tokens: {} {} {}'.format(n+2, gemType, splendorUtil.GemTypesColors[(gemTypeIndex+1)%5], splendorUtil.GemTypesColors[(gemTypeIndex+2)%5]))

pprint(actions)


while True:
    randomAction = random.sample(['Buy','Bank','Reserve'],1)[0]
    print "Action: ", randomAction
    txt = raw_input('possible?')
    if txt == 'y':
        if randomAction == 'Reserve'