from game import Agent, Actions
from splendorUtil import GemType
import msvcrt, sys
import learner, util

# Expecti-minimax Agent
class NeoTDAgent( Agent ):

  def __init__(self, index, depth, weightVector):
    self.index = index # agent/player index {0,1,2,3}
    self.isKeyboardAgent = False
    self.simKeyStrokes = []
    self.lastSimKeyStrokes = []
    self.depth = depth
    self.numRecursions = 0
    self.weightVector = weightVector

  def getAgentName(self):
    return "NeoTDAgent"

  def evaluationFunction(self, gameState):
    value = util.dotProduct(self.weightVector, learner.featureExtractor(gameState))
    return value

  def getAction(self, gameState):

    # -----------------------------------------------------
    # Minimax Recursion with Alpha-Beta Pruning
    # -----------------------------------------------------
    def recurseVminmaxPrune(gameState, depth, agentIndexInTurn, alpha, beta):
      self.numRecursions += 1
      # print "depth=",depth
      if msvcrt.kbhit():
        print "numRecursions=",self.numRecursions
        sys.exit(0)

      # isEnd(s)
      if gameState.isEndState():
        return (gameState.utility(self.index), [ord('6')]) # NoOp

      # Max depth reached?
      if depth == 0:
        # print "Value(gameState)=",self.evaluationFunction(gameState)
        return (self.evaluationFunction(gameState), [ord('6')]) # NoOp

      # Is last agent/player in ply? Go one level deeper
      numAgents = len(gameState.agentStates)
      d = depth-1 if (agentIndexInTurn == (numAgents-1)) else depth

      # Is self's turn? -> Maximizer
      if agentIndexInTurn == self.index:
        # Collect legal moves.
        agentActions = gameState.getLegalActions(agentIndexInTurn)
        localMax = (float('-inf'), [ord('6')]) # NoOp
        for i,action in enumerate(agentActions):
          # print "action:{} Maximizer - AgentIndex:{} - #Actions:{} - Depth:{}".format(i,agentIndexInTurn, len(agentActions), d)
          succGameState = gameState.generateSuccessorNoChance(agentIndexInTurn, action)
          # Maximize over recursion. Also, wrap-around agentIndex to next player.
          localMax = max(localMax,
                         (recurseVminmaxPrune(succGameState, d, (agentIndexInTurn+1) % numAgents, \
                                              alpha, beta)[0], action))
          alpha = max(alpha, localMax[0])
          if alpha >= beta:
            break # Prune!
        return localMax

      # Is opponent's turn? -> Minimizer
      elif 0 <= agentIndexInTurn < numAgents:
        # Collect legal moves.
        agentActions = gameState.getLegalActions(agentIndexInTurn)
        localMin = (float('+inf'), [ord('6')]) # NoOp
        for i,action in enumerate(agentActions):
          # print "action:{} Minimizer - AgentIndex:{} - #Actions:{} - Depth:{}".format(i,agentIndexInTurn, len(agentActions), d)
          succGameState = gameState.generateSuccessorNoChance(agentIndexInTurn, action)
          # Minimize over recursion. Also, wrap-around agentIndex to next player.
          localMin = min(localMin,
                         (recurseVminmaxPrune(succGameState, d, (agentIndexInTurn+1) % numAgents, \
                                              alpha, beta)[0], action))
          beta = min(beta, localMin[0])
          if alpha >= beta:
            break # Prune!
        return localMin

      else:
        raise Exception("Unknown agentIndexInTurn: #{}".format(agentIndexInTurn))


    # value, action = recurseVexptminmax(gameState, self.depth, self.index, None)
    # value, action = recurseVminmax(gameState, self.depth, self.index)
    # print "self.depth=",self.depth
    # Start alpha and beta with their worst possible score. (alpha=-inf, beta=+inf)
    value, action = recurseVminmaxPrune(gameState, self.depth, self.index, float('-inf'), float('inf'))
    # print 'NeoTDAgent says action = {}, value = {}'.format(action, value)
    # print "numRecursions=",self.numRecursions

    # Remember action picked
    self.simKeyStrokes = action
    self.lastSimKeyStrokes = list(action)
    return action
