"""
splendor.py holds the logic for the Splendor board game along with the main
code to run a game.
"""
import sys, types, time, random, os
from game import Cost, DevelopmentCard, NobleTile, AgentState, TableState, Game
from pprint import pprint
import splendorUtil
from splendorUtil import GemType, GemTypesColors, NobleTilesAll
from BaselineAgent import BaselineAgent
from RandomAgent import RandomAgent
from KeyboardAgent import KeyboardAgent
from SmithAgent import SmithAgent
from NeoAgent import NeoAgent
from NeoTDAgent import NeoTDAgent
from TDLearnAgent import TDLearnAgent
import datetime
import cPickle
from collections import Counter
import collections

################################################################################
#                             SPLENDOR BOARD GAME LOGIC                        #
################################################################################

#===============================================================================
def createNobleTilesForTableSetup(numPlayers):
  nobleTilesIndexes = splendorUtil.setupTableNobles(numPlayers)
  prestige = splendorUtil.NoblePrestigePoints

  nobleTiles = []
  for index in nobleTilesIndexes:
    gemRequirements = [(requirement[1], GemType[requirement[0]]) \
                        for requirement in NobleTilesAll[index]  ]
    nobleTile = NobleTile(prestige, Cost(gemRequirements))
    nobleTiles.append(nobleTile)

  return nobleTiles

#===============================================================================
def createDecks():
  decksAsTuples = splendorUtil.generateDeck()
  decks = []
  for deckAsTuples in decksAsTuples:
    deck = []
    for devCardTuple in deckAsTuples:
      tier = devCardTuple[0]
      gemType = GemType[devCardTuple[1]]
      prestige = devCardTuple[2]
      gemCosts = [ (gemCost[1], GemType[gemCost[0]]) for gemCost in devCardTuple[3] ]
      cost = Cost(gemCosts)
      deck.append( DevelopmentCard(tier, gemType, prestige, cost) )
    # Shuffle each deck separately
    random.shuffle(deck)
    decks.append(deck)
  return decks

################################################################################
#                             FRAMEWORK TO START A GAME                        #
################################################################################

#===============================================================================
def default(str):
  return str + ' [Default: %default]'

def parseAgentArgs(str):
  if str == None: return {}
  pieces = str.split(',')
  opts = {}
  for p in pieces:
    if '=' in p:
      key, val = p.split('=')
    else:
      key,val = p, 1
    opts[key] = val
  return opts

#===============================================================================
def readCommand( argv ):
  """
  Processes the command used to run splendor from the command line.
  """
  from optparse import OptionParser
  usageStr = """
  USAGE:      python splendor.py <options>
  EXAMPLES:   (1) python splendor.py
                  - starts an interactive game
              (2) python splendor.py -r
                  - starts an interactive game on a smaller board
  """
  parser = OptionParser(usageStr)

  parser.add_option('-n', '--numGames', dest='numGames', type='int',
                    help=default('the number of GAMES to play'), metavar='GAMES', default=1)
  parser.add_option('-s', '--splendor', dest='splendor',
                    help=default('the agent TYPE in the splendorAgents module to use'),
                    metavar='TYPE', default='KeyboardAgent')
  parser.add_option('-t', '--textGraphics', action='store_true', dest='textGraphics',
                    help='Display output as text only', default=False)
  parser.add_option('-q', '--quietTextGraphics', action='store_true', dest='quietGraphics',
                    help='Generate minimal output and no graphics', default=False)
  parser.add_option('-v', '--verbose', action='store_true', dest='verbose',
                    help='Generate output printing stats', default=False)
  parser.add_option('-l', '--learn', action='store_true', dest='learn',
                    help='Use agent that executes the TD-Learning Policy.', default=False)
  parser.add_option('-g', '--ghosts', dest='ghost',
                    help=default('the ghost agent TYPE in the ghostAgents module to use'),
                    metavar = 'TYPE', default='RandomGhost')
  parser.add_option('-k', '--numopps', type='int', dest='numOpps',
                    help=default('The number of opponents to use'), default=1)
  parser.add_option('-d', '--depth', type='int', dest='depth',
                    help=default('The search depth-limit. Look ahead param.'), default=1)
  parser.add_option('-z', '--zoom', type='float', dest='zoom',
                    help=default('Zoom the size of the graphics window'), default=1.0)
  parser.add_option('-f', '--fixRandomSeed', action='store_true', dest='fixRandomSeed',
                    help='Fixes the random seed to always play the same game', default=False)
  parser.add_option('-r', '--recordActions', action='store_true', dest='record',
                    help='Writes game histories to a file (named by the time they were played)', default=False)
  parser.add_option('--replay', dest='gameToReplay',
                    help='A recorded game file (pickle) to replay', default=None)
  parser.add_option('-a','--agentArgs',dest='agentArgs',
                    help='Comma separated values sent to agent. e.g. "opt1=val1,opt2,opt3=val3"')
  parser.add_option('-x', '--numTraining', dest='numTraining', type='int',
                    help=default('How many episodes are training (suppresses output)'), default=0)
  parser.add_option('--frameTime', dest='frameTime', type='float',
                    help=default('Time to delay between frames; <0 means keyboard'), default=0.1)
  parser.add_option('-c', '--catchExceptions', action='store_true', dest='catchExceptions', 
                    help='Turns on exception handling and timeouts during games', default=False)
  parser.add_option('--timeout', dest='timeout', type='int',
                    help=default('Maximum length of time an agent can spend computing in a single game'), default=30)

  options, otherjunk = parser.parse_args(argv)
  if len(otherjunk) != 0:
    raise Exception('Command line input not understood: ' + str(otherjunk))
  args = dict()

  # Record game?
  args['record'] = options.record
  # Fix the random seed?
  args['fixRandomSeed'] = options.fixRandomSeed
  # Quite graphics?
  args['quietGraphics'] = options.quietGraphics
  # Verbose?
  args['verbose'] = options.verbose
  # Executing TD-Learning policy?
  args['learn'] = options.learn
  # Number of games to play?
  args['numGames'] = options.numGames
  # Number of opponents?
  args['numOpps'] = options.numOpps
  # Search Depth-Limit?
  args['depth'] = options.depth


  # Choose a Splendor agent
  # noKeyboard = options.gameToReplay == None and (options.textGraphics or options.quietGraphics)
  # splendorType = loadAgent(options.splendor, noKeyboard)
  # agentOpts = parseAgentArgs(options.agentArgs)
  # if options.numTraining > 0:
  #   args['numTraining'] = options.numTraining
  #   if 'numTraining' not in agentOpts: agentOpts['numTraining'] = options.numTraining
  # splendor = splendorType(**agentOpts) # Instantiate Splendor with agentArgs
  # args['splendor'] = splendor

  # Don't display training games
  # if 'numTrain' in agentOpts:
  #   options.numQuiet = int(agentOpts['numTrain'])
  #   options.numIgnore = int(agentOpts['numTrain'])

  # Choose a ghost agent
  # ghostType = loadAgent(options.ghost, noKeyboard)
  # args['ghosts'] = [ghostType( i+1 ) for i in range( options.numGhosts )]

  # Choose a display format
  # if options.quietGraphics:
  #     import textDisplay
  #     args['display'] = textDisplay.NullGraphics()
  # elif options.textGraphics:
  #   import textDisplay
  #   textDisplay.SLEEP_TIME = options.frameTime
  #   args['display'] = textDisplay.SplendorGraphics()
  # else:
  #   import graphicsDisplay
  #   args['display'] = graphicsDisplay.SplendorGraphics(options.zoom, frameTime = options.frameTime)
  # args['numGames'] = options.numGames
  # args['record'] = options.record
  # args['catchExceptions'] = options.catchExceptions
  # args['timeout'] = options.timeout

  # Special case: recorded games don't use the runGames method or args structure
  # if options.gameToReplay != None:
  #   print 'Replaying recorded game %s.' % options.gameToReplay
  #   import cPickle
  #   f = open(options.gameToReplay)
  #   try: recorded = cPickle.load(f)
  #   finally: f.close()
  #   recorded['display'] = args['display']
  #   replayGame(**recorded)
  #   sys.exit(0)

  return args

#===============================================================================
def loadAgent(agentType, nographics):
  # Looks through all pythonPath Directories for the right module,
  pythonPathStr = os.path.expandvars("$PYTHONPATH")
  if pythonPathStr.find(';') == -1:
    pythonPathDirs = pythonPathStr.split(':')
  else:
    pythonPathDirs = pythonPathStr.split(';')
  pythonPathDirs.append('.')

  for moduleDir in pythonPathDirs:
    if not os.path.isdir(moduleDir): continue
    moduleNames = [f for f in os.listdir(moduleDir) if f.endswith('gents.py') or f=='submission.py']
    for modulename in moduleNames:
      try:
        module = __import__(modulename[:-3])
      except ImportError:
        continue
      if agentType in dir(module):
        if nographics and modulename == 'keyboardAgents.py':
          raise Exception('Using the keyboard requires graphics (not text display)')
        return getattr(module, agentType)
  raise Exception('The agent ' + agentType + ' is not specified in any *Agents.py.')

#===============================================================================
def replayGame( layout, actions, display ):
    import submission, ghostAgents
    rules = ClassicGameRules()
    # If replaying, change the agent from ExpectimaxAgent to whatever agent with which you want to play
    agents = [submission.ExpectimaxAgent()] + [ghostAgents.RandomGhost(i+1) for i in range(layout.getNumGhosts())]
    game = rules.newGame( layout, agents[0], agents[1:], display )
    state = game.state
    display.initialize(state.data)

    for action in actions:
      # Execute the action
      state = state.generateSuccessor( *action )
      # Change the display
      display.update( state.data )
      # Allow for game specific conditions (winning, losing, etc.)
      rules.process(state, game)

    display.finish()

#===============================================================================
def runGames( layout, pacman, ghosts, display, numGames, record, numTraining = 0, catchExceptions=False, timeout=30 ):
  import __main__
  __main__.__dict__['_display'] = display

  rules = ClassicGameRules(timeout)
  games = []

  for i in range( numGames ):
    beQuiet = i < numTraining
    if beQuiet:
        # Suppress output and graphics
        import textDisplay
        gameDisplay = textDisplay.NullGraphics()
        rules.quiet = True
    else:
        gameDisplay = display
        rules.quiet = False
    game = rules.newGame(layout, pacman, ghosts, gameDisplay, beQuiet, catchExceptions)
    game.run()
    if not beQuiet: games.append(game)

    if record:
      import time, cPickle
      fname = ('recorded-game-%d' % (i + 1)) +  '-'.join([str(t) for t in time.localtime()[1:6]])
      f = file(fname, 'w')
      components = {'layout': layout, 'actions': game.moveHistory}
      cPickle.dump(components, f)
      f.close()

  if (numGames-numTraining) > 0:
    scores = [game.state.getScore() for game in games]
    wins = [game.state.isWin() for game in games]
    winRate = wins.count(True)/ float(len(wins))
    print 'Average Score:', sum(scores) / float(len(scores))
    print 'Scores:       ', ', '.join([str(score) for score in scores])
    print 'Win Rate:      %d/%d (%.2f)' % (wins.count(True), len(wins), winRate)
    print 'Record:       ', ', '.join([ ['Loss', 'Win'][int(w)] for w in wins])

  return games

#===============================================================================
if __name__ == '__main__':
  """
  The main function called when splendor.py is run
  from the command line:

  > python splendor.py

  See the usage string for more details.

  > python pacman.py --help
  """
  args = readCommand( sys.argv[1:] ) # Get game components based on input
  # runGames( **args )

  # rules = ClassicGameRules(timeout)
  # game = rules.newGame(layout, pacman, ghosts, gameDisplay, beQuiet, catchExceptions)
  # game.run()

  # Tests
  """
  A Cost in splendor is a list of Gem costs.
  Each Gem cost is a tuple of (numberOfGems, gemColor, gemType)
      e.g. ( 3, Green, Emeralds )
  """
  record = args['record'] # defaults to False
  fixRandomSeed = args['fixRandomSeed'] # defaults to False
  quietGraphics = args['quietGraphics'] # defaults to False
  verbose = args['verbose'] # defaults to False
  learn = args['learn'] # defaults to False
  numGames = args['numGames'] # defaults to 1
  numOpps = args['numOpps'] # defaults to 1
  depth = args['depth'] # defaults to 1

  if verbose:
    startDT = datetime.datetime.now()
    print "Start Time:", startDT

  prestigeWinnerList = []
  roundsList = []
  winnerList = []
  for i in range(numGames):
    currentDT = datetime.datetime.now()
    if verbose:
      print "New Game #{} Time: {}".format(i+1,str(currentDT))

    if fixRandomSeed:
      random.seed('cs221')
    else:
      random.seed( 'cs221'+str(currentDT)+str(os.getpid()) )
      # random.seed('cs221'+str(i))

    # So far -->  RandomAgent < BaselineAgent < SmithAgent
    # agents = [BaselineAgent(0), RandomAgent(1), RandomAgent(2), RandomAgent(3)]
    # agents = [BaselineAgent(0), RandomAgent(1)]
    # agents = [KeyboardAgent(0), BaselineAgent(1)]
    # agents = [SmitAhgent(0), BaselineAgent(1), RandomAgent(2), BaselineAgent(3)]
    # agents = [KeyboardAgent(0), SmithAgent(1)]
    # agents = [NeoAgent(0), SmithAgent(1), BaselineAgent(2), RandomAgent(3)]
    if learn:
      agents = [TDLearnAgent(0, 0.01), TDLearnAgent(1, 0.01)]

    else:
      # agents = [NeoAgent(0, depth), NeoAgent(1, depth)]
      filename = 'weights'
      if os.path.isfile(filename):
        f = open(filename, "r")
        try: data = cPickle.load(f)
        finally: f.close()
        weightVector = data['weights']
      # No previous experience. Load all Zeros.
      else:
        weightVector = collections.defaultdict(float)

      # Feature Set #2 -> 62% - 70%
      # agents = [BaselineAgent(0), NeoTDAgent(1, depth, weightVector)]

      # Feature Set #2 -> 89.7% - 61%
      # agents = [RandomAgent(0), NeoTDAgent(1, depth, weightVector)]

      # Feature Set #2 -> 51% - 26%
      # agents = [NeoAgent(0, depth), NeoTDAgent(1, depth, weightVector)]

      # Feature Set #2+3 -> 68% - 28%
      # agents = [NeoTDAgent(0, depth, weightVector), BaselineAgent(1)]

      agents = [KeyboardAgent(0), BaselineAgent(1)]
      # agents = [NeoTDAgent(0, depth, weightVector), SmithAgent(1)]
      # agents = [NeoTDAgent(0, depth, weightVector), NeoAgent(1, 1)]

      # agents = [BaselineAgent(0), RandomAgent(1)]
      # agents = [NeoAgent(0, depth), RandomAgent(1)]
      # agents = [NeoAgent(0, depth), NeoAgent(1, depth)]


    # NeoAgent must be index 0
    decks = createDecks()

    devCards = []
    for deck in decks:
      # Shuffle each deck
      random.shuffle(deck)
      # Draw the top 4 cards of each deck, face-up on the table.
      devCardsTier = [deck.pop(0) for _ in range(4)]
      devCards.append(devCardsTier)

    nobleTiles = createNobleTilesForTableSetup(len(agents))
    tokenMultiplicity = splendorUtil.getTokenMultiplicity(len(agents))
    numberOfJokers = 5
    tableState = TableState(devCards, decks, nobleTiles, tokenMultiplicity, numberOfJokers)
    startingIndex = 0
    prestigeToWin = 15
    game = Game(not quietGraphics, agents, tableState, startingIndex, prestigeToWin)
    gameOver, prestige, rounds, winner = game.run()

    if gameOver:
      prestigeWinnerList.append(prestige)
      roundsList.append(rounds)
      winnerList.append(winner)
      # print "Episode:"
      # pprint(game.episode)
      if record:
        import time, cPickle
        fname = ('genTrainData/recorded-game-%d-' % (i + 1)) + '-'.join([str(t) for t in time.localtime()[1:10]]) + str(os.getpid())
        f = file(fname, 'w')
        components = {'episode': game.episode}
        cPickle.dump(components, f)
        f.close()

  # print results if at least 1 finished game had a winner.
  if verbose and len(winnerList) != 0:
    print "------------------------"
    print "Number of Games: {}".format( numGames )
    print "Prestige Avg: {}".format( sum(prestigeWinnerList)/len(prestigeWinnerList) )
    print "Rounds   Avg: {}".format( sum(roundsList)/len(roundsList) )
    print "Winners: ", Counter(winnerList)
    for i,agent in enumerate(agents):
      print "Agent[{}] = {}".format(i, agent.getAgentName())

  if verbose:
    endDT = datetime.datetime.now()
    elapsedDT = endDT - startDT
    print "End Time:", endDT
    print "Elapsed Time:", elapsedDT

  pass